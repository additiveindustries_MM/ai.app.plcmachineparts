﻿using System;
using System.Reflection;
using System.Xml;
using AI.Lib.DatabaseConstants;
using AI.Lib.DatabaseConstants.MachineParts;

namespace PlcMachineParts.CiFile
{
	public class CiFile
	{
		protected XmlNode GetInfoNode(XmlDocument xmlDoc)
		{
			XmlNode infoNode = xmlDoc.CreateElement("info");

			XmlElement id = xmlDoc.CreateElement("id");
			id.SetAttribute("amsnetid", Program.Plc.Address);
			id.SetAttribute("pcname", Environment.MachineName);
			infoNode.AppendChild(id);

			XmlElement db = xmlDoc.CreateElement("version");
			db.SetAttribute("messagemaster", Assembly.GetEntryAssembly().GetName().Version.ToString());
			infoNode.AppendChild(db);

			XmlElement date = xmlDoc.CreateElement("date");
			date.SetAttribute("utc", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"));
			date.SetAttribute("local", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
			infoNode.AppendChild(date);

			return infoNode;
		}


		protected MachinePartDefinition GetMachinePartDefinition(XmlNode mp)
		{
			ModuleDefinition md = AI.Lib.EnterpriseDefines.EnumConverters.ParseModuleDefinition(mp.Attributes["defmod"].Value);
			UInt16 mpdefid = UInt16.Parse(mp.Attributes["machinepartDefinitionid"].Value);
			string name = mp.Attributes["name"].Value;

			return new MachinePartDefinition(md, mpdefid, name, name);
		}

		protected MachinePart GetMachinePart(XmlNode mp)
		{
			ModuleDefinition defmod = AI.Lib.EnterpriseDefines.EnumConverters.ParseModuleDefinition(mp.Attributes["defmod"].Value);
			ModuleDefinition plcdefmod = AI.Lib.EnterpriseDefines.EnumConverters.ParseModuleDefinition(mp.Attributes["plcdefmod"].Value);
			UInt16 machprtid = UInt16.Parse(mp.Attributes["machprtid"].Value);
			UInt16 parentmachprtid = UInt16.Parse(mp.Attributes["parentmachprtid"].Value);
			UInt16 machinepartdefinitionid = UInt16.Parse(mp.Attributes["machinepartdefinitionid"].Value);

			UInt16 assycode = UInt16.Parse(mp.Attributes["assycode"].Value);
			UInt16 msgoffset = UInt16.Parse(mp.Attributes["msgoffset"].Value);

			string descr = mp.Attributes["descr"].Value;

			return new MachinePart(plcdefmod, defmod, machprtid, parentmachprtid, machinepartdefinitionid, assycode, msgoffset, "", descr);
		}

/*
		protected XmlElement MachinePart(XmlDocument xmlDoc, TreeMachinePartNode node)
		{
			XmlElement machinePart = xmlDoc.CreateElement("machinepart");
			machinePart.SetAttribute("defmod", node.Part.DefModule.ToString());
			machinePart.SetAttribute("plcdefmod", node.Part.PlcDefModule.ToString());
			machinePart.SetAttribute("name", node.PartDefinition.MachinePartDefinitionName); // from def
			machinePart.SetAttribute("machprtid", node.Part.MachinePartId.ToString());
			machinePart.SetAttribute("parentmachprtid", node.Part.ParentMachinePartId.ToString());
			machinePart.SetAttribute("machinepartdefinitionid", node.Part.MachinePartDefinitionId.ToString());
			machinePart.SetAttribute("assycode", node.Part.AssemblyCode.ToString());
			machinePart.SetAttribute("msgoffset", node.Part.MessageOffset.ToString());
			machinePart.SetAttribute("descr", node.Part.Description);
			machinePart.SetAttribute("parttype", node.Part.PartType.ToString());
			return machinePart;
		}

		protected XmlElement MachinePartDefinitionNode(XmlDocument xmlDoc, TreeMachinePartNode node)
		{
			XmlElement machinePartDef = xmlDoc.CreateElement("machinepartdefinition");
			machinePartDef.SetAttribute("defmod", node.PartDefinition.DefModule.ToString());
			machinePartDef.SetAttribute("machinepartdefinitionid", node.PartDefinition.MachinePartDefinitionId.ToString());
			machinePartDef.SetAttribute("name", node.PartDefinition.MachinePartDefinitionName);
			return machinePartDef;
		}
		*/
	}
}
