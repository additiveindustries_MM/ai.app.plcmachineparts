﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using AI.Lib.DatabaseConstants;
using AI.Lib.DatabaseConstants.MachineParts;
using AI.Lib.Global;

namespace PlcMachineParts.CiFile
{
/*
	public class CiFileNewMsg : PlcMachineParts.CiFile.CiFile
	{
		const string nodetext_root = "machinepartedits";
		const string nodetext_machinepartdefinitionsnew = "machinepartdefinitionsnew";
		const string nodetext_machinepartsnew = "machinepartsnew";
		const string nodetext_messagesnew = "messagesnew";
		const string nodetext_messagesedit = "messagesedit";

		#region Read

		public bool Read(string path)
		{
			XmlDocument doc = new XmlDocument();
			try
			{
				doc.Load(path);
			}
			catch (Exception ex)
			{
				Log.Message(Severity.ERROR, $"File not prensent: {path}: {ex.Message}");
				return false;
			}

			XmlNode xmlNode = doc.DocumentElement.SelectSingleNode($"/{nodetext_root}/{nodetext_machinepartdefinitionsnew}");
			List<MachinePartDefinition> newmachPrtDefs = Read_NewMachPrtDefs(xmlNode);

			xmlNode = doc.DocumentElement.SelectSingleNode($"/{nodetext_root}/{nodetext_machinepartsnew}");
			if (!Read_NewMachPrts(xmlNode, newmachPrtDefs))
			{
				Log.Message(Severity.ERROR, $"File content [/{nodetext_root}/{nodetext_machinepartsnew}] not correct: {path}");
				return false;
			}

			xmlNode = doc.DocumentElement.SelectSingleNode($"/{nodetext_root}/{nodetext_messagesnew}");
			if (!Read_NewMessagesForExistingMachPrts(xmlNode))
			{
				Log.Message(Severity.ERROR, $"File content [/{nodetext_root}/{nodetext_messagesnew}] not correct: {path}");
				return false;
			}

			xmlNode = doc.DocumentElement.SelectSingleNode($"/{nodetext_root}/{nodetext_messagesedit}");
			if (!Read_ExistingMessageEdits(xmlNode))
			{
				Log.Message(Severity.ERROR, $"File content [/{nodetext_root}/{nodetext_messagesedit}] not correct: {path}");
				return false;
			}
			return true;
		}

		private bool Read_NewMachPrts(XmlNode mpNode, List<MachinePartDefinition> newmachPrtDefs)
		{
			foreach (XmlNode xmlNode in mpNode.ChildNodes) 
			{
				UInt32 id = UInt32.Parse(xmlNode.Attributes["machprtid"].Value);
				TreeMachinePartNode treenode = Program.MainForm.GetTree(TreeType.Symbolic).GetNode(id);
				if (treenode == null) // should be null
				{
					MachinePart mp = GetMachinePart(xmlNode);

					MachinePartDefinition mpdef = null;
					if (newmachPrtDefs.FindIndex(x => x.MachinePartDefinitionId == mp.MachinePartDefinitionId) > -1)
						mpdef = newmachPrtDefs.FirstOrDefault(x => x.MachinePartDefinitionId == mp.MachinePartDefinitionId);
					// mpdef might be null, but then while creating TreeMachinePartNode tried to lookup

					TreeMachinePartNode tmpn = new TreeMachinePartNodeSym(mp);

					TreeMachinePartNode treenodeParent = Program.MainForm.GetTree(TreeType.Symbolic).GetNode(tmpn.Part.ParentMachinePartId);
					treenodeParent.Nodes.Add(tmpn);
					treenodeParent.Expand();
					Log.Message(Severity.WARNING, "Should add " + tmpn);
				}
				else
				{
					Log.Message(Severity.ERROR, $"machinepart {treenode.Name}[{treenode.MachinePartId}] can not be added, it already exists");
				}
			}
			return true;
		}

		private List<MachinePartDefinition> Read_NewMachPrtDefs(XmlNode mpNode)
		{
			List<MachinePartDefinition> newmachPrtDefs = new List<MachinePartDefinition>();
			if (mpNode == null)
				return newmachPrtDefs;

			foreach (XmlNode mp in mpNode.ChildNodes) // machinepartdefs
			{
				ModuleDefinition md = AI.Lib.EnterpriseDefines.EnumConverters.ParseModuleDefinition(mp.Attributes["defmod"].Value);
				UInt16 mpdefid = UInt16.Parse(mp.Attributes["machinepartdefinitionid"].Value);
				string name = mp.Attributes["name"].Value;

				MachinePartDefinition mpd = new MachinePartDefinition(md, mpdefid, name);
				newmachPrtDefs.Add(mpd);
			}
			return newmachPrtDefs;
		}

		private bool Read_NewMessagesForExistingMachPrts(XmlNode mpNode)
		{
			if (mpNode == null)
				return false;

			foreach (XmlNode mp in mpNode.ChildNodes) // {nodetext_messagesnew}
			{
				UInt32 id = UInt32.Parse(mp.Attributes["machprtid"].Value);
				TreeMachinePartNode treenode = Program.MainForm.GetTree(TreeType.Plc).GetNode(id);
				Read_UpdateNewMessagesForThisMachPart(treenode, mp.ChildNodes);
			}

			return true;
		}

		private bool Read_ExistingMessageEdits(XmlNode mpNode)
		{
			if (mpNode == null)
				return false;

			foreach (XmlNode mp in mpNode.ChildNodes) // configuration
			{
				UInt32 id = UInt32.Parse(mp.Attributes["machprtid"].Value);
				TreeMachinePartNode treenode = Program.MainForm.GetTree(TreeType.Plc).GetNode(id);
				Read_UpdateNewMessagesForThisMachPart(treenode, mp.ChildNodes);
			}

			return true;
		}

		private void Read_UpdateNewMessagesForThisMachPart(TreeMachinePartNode _treenode, XmlNodeList _machPrt)
		{
			foreach (XmlNode msg in _machPrt)
			{
				UInt32 msgid = UInt32.Parse(msg.Attributes["msgid"].Value);

				IMachinePartDefMessage lvi = _treenode.AllMessages.FirstOrDefault(x => x.MessageId == msgid);
				if (lvi != null)
				{
					if (msg.Attributes["short"] != null)
					{
						string shortmsg = msg.Attributes["short"].Value;
						if (!String.IsNullOrEmpty(shortmsg) && !lvi.ShortName.Equals(shortmsg))
						{
							Debug.WriteLine($"Update to shortmsg from [{lvi.ShortName}] to [{shortmsg}]");
//							lvi.MachinePartDefinitionMessage_UpdateShortName(shortmsg);
						}
					}

					if (msg.Attributes["display"] != null)
					{
						string displayname = msg.Attributes["display"].Value;
						if (!String.IsNullOrEmpty(displayname) &&  !lvi.DisplayName.Equals(displayname))
						{
							Debug.WriteLine($"Update to displayname from [{lvi.DisplayName}] to [{displayname}]");
//							lvi.MachinePartDefinitionMessage_UpdateDisplayName(displayname);
						}
					}

					if (msg.Attributes["cat"] != null)
					{
						MessageCategory cat = (MessageCategory)Enum.Parse(typeof(MessageCategory), msg.Attributes["cat"].Value, true);
						if (cat != MessageCategory.Unknown && !lvi.NotificationType.Equals(cat))
						{
							Debug.WriteLine($"Update to cat from [{lvi.NotificationType}] to [{cat}]");
//							lvi.MachinePartDefinitionMessage_UpdateCategory(cat);
						}
					}
				}
			}
		}


		#endregion

		#region Write
		public bool Write(string path)
		{
			XmlDocument xmlDoc = new XmlDocument();

			XmlNode rootNode = xmlDoc.CreateElement(nodetext_root); // was newmessages, now machinepartedits
			xmlDoc.AppendChild(rootNode);

			rootNode.AppendChild(GetInfoNode(xmlDoc));

			XmlNode newmachinePartDefinitions = xmlDoc.CreateElement(nodetext_machinepartdefinitionsnew);
			rootNode.AppendChild(newmachinePartDefinitions);
			// now add a node for each new machinepartdefinition
			Program.MainForm.GetTree(TreeType.Symbolic).ExecuteOnAllNodes(f => !f.IsResolved, x => Write_GetNewMachPartDefinitions(xmlDoc, newmachinePartDefinitions, x));

			XmlNode newmachineParts = xmlDoc.CreateElement(nodetext_machinepartsnew);
			rootNode.AppendChild(newmachineParts);
			// now add a node for each new machinepartdefinition
			Program.MainForm.GetTree(TreeType.Symbolic).ExecuteOnAllNodes(f => !f.IsResolved, x => Write_GetNewMachParts(xmlDoc, newmachineParts, x));

			// Machineparts are already known, so not needed to write in file. But only needed for their new messages
			XmlNode machineParts = xmlDoc.CreateElement(nodetext_messagesnew);
			Program.MainForm.GetTree(TreeType.Plc).ExecuteOnAllNodes(f => f.IsResolved && f.HasNewMessages, x => Write_GetNewMessagesForThisMachPart(xmlDoc, machineParts, x));
			rootNode.AppendChild(machineParts);

			XmlNode edits = xmlDoc.CreateElement(nodetext_messagesedit);
			rootNode.AppendChild(edits);
			// now add a node for each machinepart that has new messages and add these
			Program.MainForm.GetTree(TreeType.Plc).ExecuteOnAllNodes(f => f.IsResolved && f.HasMessages, x => Write_GetEditsForThisMachPart(xmlDoc, edits, x));

			xmlDoc.Save(path);
			return true;
		}

		public void Write_GetNewMachPartDefinitions(XmlDocument xmlDoc, XmlNode newmachinePartdefs, TreeMachinePartNode node)
		{
			if (node.IsMachinePartDefinitionResolved) 
				return; // machinepartDefinition already in database

			XmlElement machinePartdef = MachinePartDefinitionNode(xmlDoc, node);
			newmachinePartdefs.AppendChild(machinePartdef);
		}

		public void Write_GetNewMachParts(XmlDocument xmlDoc, XmlNode newmachineParts, TreeMachinePartNode node)
		{
			XmlElement newmachinePart = MachinePart(xmlDoc, node);
			newmachineParts.AppendChild(newmachinePart);
		}

		public void Write_GetNewMessagesForThisMachPart(XmlDocument xmlDoc, XmlNode machineParts, TreeMachinePartNode node)
		{
			XmlElement machinePart = MachinePart(xmlDoc, node);

			foreach (ListViewMessagesItem lvi in node.AllMessages.Where(x => x.State == EditState.Added))
			{
				XmlElement msg = xmlDoc.CreateElement("msg");
				msg.SetAttribute("msgid", lvi.Msg.MessageId.ToString());
				msg.SetAttribute("short", lvi.Msg.ShortName);
				msg.SetAttribute("display", lvi.Msg.DisplayName);
				msg.SetAttribute("cat", lvi.Msg.NotificationType.ToString());
				machinePart.AppendChild(msg);
			}

			machineParts.AppendChild(machinePart);
		}
		public void Write_GetEditsForThisMachPart(XmlDocument xmlDoc, XmlNode machineParts, TreeMachinePartNode node)
		{
			XmlElement machinePart = MachinePart(xmlDoc, node);

			foreach (ListViewMessagesItem lvi in node.AllMessages.Where(x => x.State == EditState.Edited))
			{
				XmlElement msg = xmlDoc.CreateElement("msg");
				msg.SetAttribute("msgid", lvi.Msg.MessageId.ToString());
/*
				if (lvi.GetContentEditedFlag(ListViewMessagesItem.DataContentEdited.ShortName))
					msg.SetAttribute("short", lvi.Msg.ShortName);
				if (lvi.GetContentEditedFlag(ListViewMessagesItem.DataContentEdited.DisplayName))
					msg.SetAttribute("display", lvi.Msg.DisplayName);
				if (lvi.GetContentEditedFlag(ListViewMessagesItem.DataContentEdited.NotificationType))
					msg.SetAttribute("cat", lvi.Msg.NotificationType.ToString());
#1#

				if (msg.Attributes.Count > 1)  // msgId is always added
					machinePart.AppendChild(msg);
			}
			if (machinePart.ChildNodes.Count > 0)  // only add when msg
				machineParts.AppendChild(machinePart);
		}

		#endregion
	}
*/
}
