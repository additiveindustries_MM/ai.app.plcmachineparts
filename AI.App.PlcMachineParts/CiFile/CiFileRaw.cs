﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using AI.Lib.Global;

namespace PlcMachineParts.CiFile
{
	public class CiFileRaw : PlcMachineParts.CiFile.CiFile
	{
//		public List<MachinePartPlc> RepoMachineParts { get; } = new List<MachinePartPlc>();
/*
		public event EventHandler<EventArgs> OnRepoMachinePartsChanged;
		private void FireOnRepoMachinePartsChanged()
		{
			EventHandler<EventArgs> handler = OnRepoMachinePartsChanged;
			handler?.Invoke(this, new EventArgs());
		}
*/

/*
		public List<MessagePlc> MessageList  { get; }  = new List<MessagePlc>();

		public event EventHandler<EventArgs> OnMessageListChanged;
		private void FireOnMessageListChanged()
		{
			EventHandler<EventArgs> handler = OnMessageListChanged;
			handler?.Invoke(this, new EventArgs());
		}
*/

		public List<MachinePartPlc> Read(string path)
		{
			XmlDocument doc = new XmlDocument();
			try
			{
				doc.Load(path);
			}
			catch (Exception ex)
			{
				Log.Message(Severity.ERROR, $"File not prensent: {path}: {ex.Message}");
				return null;
			}

			#region MachPartCollection
			XmlNode mpNode = doc.DocumentElement.SelectSingleNode("/ciraw/machineparts");

			if (mpNode == null || String.IsNullOrEmpty(mpNode.InnerText))
			{
				Log.Message(Severity.ERROR, $"File content [/ciraw/machineparts] not correct: {path}");
				return null;
			}
			List<MachinePartPlc> repoMachineParts = Program.Plc.ParseMachineParts( mpNode.InnerText);

			#endregion

			#region Messages
			XmlNode msgNode = doc.DocumentElement.SelectSingleNode("/ciraw/messages");

			if (msgNode != null && !String.IsNullOrEmpty(msgNode.InnerText))
			{
				List<MessagePlc> messages = Program.Plc.ParseMessages(msgNode.InnerText);

				foreach (MessagePlc message in messages)
				{
                    if (repoMachineParts.Any(x => x.MachinePartId == message.MachinePartId))
                    {
                        MachinePartPlc machinePartPlc = repoMachineParts.First(x => x.MachinePartId == message.MachinePartId);

                        if (machinePartPlc.Messages.Any(x => x.MessageId == message.MessageId))
                        {
                            Log.LogWarning($"Message was already present in machinepart: {message}");
						}
						else
                        {
                            machinePartPlc.Messages.Add(message);
						}
					}
					else
						Log.LogWarning($"Message has no machinepart: {message}");
				}
			}
			else
			{
				Log.Message(Severity.ERROR, $"File content [/ciraw/messages] not correct: {path}");
			}
			#endregion
			return repoMachineParts;
		}
		
		public bool Write(List<MachinePartPlc> allMachineParts, string path)
		{
			XmlDocument xmlDoc = new XmlDocument();
			XmlNode rootNode = xmlDoc.CreateElement("ciraw");
			xmlDoc.AppendChild(rootNode);

			// basic info
			XmlNode infoNode = GetInfoNode(xmlDoc);
			// add some extra info
			XmlElement count = xmlDoc.CreateElement("count");
			count.SetAttribute("parts", allMachineParts?.Count.ToString() ?? "NULL");
			count.SetAttribute("msgs", allMachineParts.Select(list => list.Messages.Count).Count().ToString() ?? "NULL");
			infoNode.AppendChild(count);

			rootNode.AppendChild(infoNode);

			XmlNode machineParts = xmlDoc.CreateElement("machineparts");
			rootNode.AppendChild(machineParts);

			StringBuilder MachineParts = new StringBuilder();
			StringBuilder Messages = new StringBuilder();
			Messages.AppendLine(); // Just so we don't start with <![CDATA[ on data line
			MachineParts.AppendLine(); // Just so we don't start with <![CDATA[ on data line
			if (allMachineParts?.Count != 0)
			{
				foreach (MachinePartPlc part in allMachineParts)
				{
					MachineParts.AppendLine(part.ToRawString());

					foreach (MessagePlc msg in part.Messages)
					{
						Messages.AppendLine(msg.ToRawString());
					}
				}
				XmlCDataSection cdataParts = xmlDoc.CreateCDataSection(MachineParts.ToString());
				machineParts.AppendChild(cdataParts);

				//--------------------------

				XmlNode mesages = xmlDoc.CreateElement("messages");
				rootNode.AppendChild(mesages);

				XmlCDataSection cdataMsg = xmlDoc.CreateCDataSection(Messages.ToString());
				mesages.AppendChild(cdataMsg);

			}
			// backslashes in path ot allowed in xml, txt dump is enough
			xmlDoc.Save(path);
			return true;
		}

	}
}
