﻿using System;
using System.Collections.Generic;
using System.Linq;
using AI.App.PlcMachineParts.DataModel;
using AI.Lib.DatabaseConstants;

namespace PlcMachineParts.CiFile
{
	public class MachinePartPlc
	{
		public ModuleDefinition ModDefDbMachinePart =>  DbMachinePart != null ?((ModuleDefinition)DbMachinePart.MODULEDEF_ID) : ModuleDefinition.Undefined;
		public ModuleDefinition ModDefDbMachinePartPlc => DbMachinePart != null ?((ModuleDefinition)DbMachinePart.PLC_MODULEDEF_ID) : ModuleDefinition.Undefined;
		public ModuleDefinition ModDefDbMachinePartDefinition => DbMachinePartDefinition != null ?((ModuleDefinition)DbMachinePartDefinition.MODULEDEF_ID) : ModuleDefinition.Undefined;



        public string ModDefDbMachinePartTxt => DbMachinePart != null ? Global.ModuleDefinitionToString(DbMachinePart.MODULEDEF_ID) : Global.ModuleDefinitionToString(0);
        public string ModDefDbMachinePartPlcTxt => DbMachinePart != null ? Global.ModuleDefinitionToString(DbMachinePart.PLC_MODULEDEF_ID) : Global.ModuleDefinitionToString(0);
        public string ModDefDbMachinePartDefinitionTxt => DbMachinePartDefinition != null ? Global.ModuleDefinitionToString(DbMachinePartDefinition.MODULEDEF_ID) : Global.ModuleDefinitionToString(0);

		public int MessageOffset => DbMachinePart?.MSGOFFSET ?? -1;
		public int AssemblyCode => DbMachinePart?.ASSEMBLYCODE ?? -1;

		public string DescrPart => DbMachinePart?.DESCR ?? "";
		public string DescrDef => DbMachinePartDefinition?.DESCR ?? "";

		public ec_machineparts DbMachinePart { get; set; }
		public ec_machinepartdefinitions DbMachinePartDefinition { get; set; }

		public MachinePartPlc MachinePartParent { get; set; }
		public List<MessagePlc> Messages { get; } = new List<MessagePlc>();

		public bool HasMachinePartParent => MachinePartParent != null;
		public long MachinePartParentId => DbMachinePart?.PARENTMACHINEPART_ID??0;

		public bool HasMachinePart => DbMachinePart != null;
		public bool HasMachinePartDefinition => DbMachinePartDefinition != null;
		public long MachinePartDefinitionId => DbMachinePartDefinition?.MACHINEPARTDEFINITION_ID?? -1;

		public ModuleDefinition DefMod { get; } = ModuleDefinition.Undefined;
		public UInt32 MachinePartId { get; }

		public int Key { get; }
		public int KeyParent { get; }

		public string MachinePath { get; }
		public string MachinePartName { get; }
		public int CountMessages => Messages?.Count??-1;
		public int CountMessagesResolved => Messages?.Count(x => x.Resolved)??-1;
		public int CountMessagesUnResolved => Messages?.Count(x => !x.Resolved)??-1;
		public int MessagesCategoryDiff =>  Messages.Count(x => x.Resolved && x.PlcMsgCategory != x.DbMsgCategory);

		public MachinePartPlc(string line)
		{
			//Log.Message(Severity.DEBUG, $"MP STRING:{str}");
			string[] parts = line.Split(new char[]{','},  StringSplitOptions.RemoveEmptyEntries);
			//Debug.Assert(parts?.Length == 4, "Bad content Get_MCF_MachinePartRepository()");

			if (parts.Length >= 4)
			{
				if (UInt32.TryParse(parts[0], out UInt32 id))
					MachinePartId = id;

				if (int.TryParse(parts[1], out int key))
					Key = key;

				if (int.TryParse(parts[2], out int keyParent))
					KeyParent = keyParent;

				MachinePath = parts[3].Trim();
				if (!MachinePath.EndsWith("/"))
					MachinePath += '/';

				if (Key == 1 && KeyParent == 1)
					KeyParent = 0;
				DefMod = FindModuleDefinitionFromPath();
			}
			else
			{
				throw new Exception($"MachinePartPlc parse error: not 4 items in[{line}] ");
			}

			// MachinePartName is only the last part
			string mpname = MachinePath;
			mpname = mpname.TrimEnd('/');
			mpname = mpname.Contains("/")? mpname.Substring(mpname.LastIndexOf('/') + 1) : mpname;
			MachinePartName = mpname;
		}

		private ModuleDefinition FindModuleDefinitionFromPath()
		{
			string s = MachinePath.Substring(0, MachinePath.IndexOf('/'));
			if (Enum.TryParse(s, true, out ModuleDefinition md))
				return md;
			else
				return ModuleDefinition.Undefined;
		}

		public string ToRawString()
		{
			return String.Format($"{this.MachinePartId},{this.Key},{this.KeyParent},{this.MachinePath}");
		}
		
		public override string ToString()
		{
			return $"[{MachinePartId}] {MachinePartName} {Key}/{KeyParent}";
		}

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
		{
			if (obj is MachinePartPlc target)
			{
				return 
				ModDefDbMachinePart == target.ModDefDbMachinePart &&
				ModDefDbMachinePartPlc == target.ModDefDbMachinePartPlc &&
				MessageOffset == target.MessageOffset &&
				AssemblyCode == target.AssemblyCode &&
				MachinePath == target.MachinePath &&
				MachinePartName  == target.MachinePartName;
			}
			return false;

		}
	}
}