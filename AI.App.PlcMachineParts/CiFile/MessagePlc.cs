﻿using System;
using System.Globalization;
using AI.App.PlcMachineParts.DataModel;
using AI.Lib.CommandInterface.Events;
using AI.Lib.DatabaseConstants;
using AI.Lib.EnterpriseDefines.CommandInterface.Events;
using AI.Lib.EnterpriseDefines.MachineInterface.Messages;
using AI.Lib.Global;

namespace PlcMachineParts.CiFile
{
	public class MessagePlc
	{
		public ec_machinepartdefinitionmessages DbMessage { get; set; }

		public string MsgFullText => DbMessage?.FULLTEXT ?? "";
		public string MsgShortName => DbMessage?.SHORTNAME ?? "";
		public string Descr => DbMessage?.DESCR ?? "-";

		public bool Resolved => DbMessage != null;
		public ModuleDefinition DefModule { get; private set; }

		public UInt32 Id { get; private set; }
		public UInt32 ResetCmdId { get; private set;}
		public DateTime TimeStamp { get; private set;}
		public UInt16 MachinePartId { get; private set;}
		public UInt16 MessageId { get; private set;}
		public string InfoString { get; private set; }
		public string Parameter { get; private set;}
		public MessageCategory PlcMsgCategory { get; private set;}
		public MessageCategory DbMsgCategory => (MessageCategory)(DbMessage?.MESSAGECAT_ID ??  0);

		public MessagePlc(string line)
		{
			//ModDef,  Id,  MachinePartId,  MessageId,  TimeStamp,           ResetCmdId,  Parameter, InfoString
			//  0     1         2             3           4                  5            6          7                          
			// AMC, 65536021, 1000,           21,       2018-03-14 07:09:54, 0,           EvTestMsg, 2018-03-14 07:09:54.530:   03E80015 03E80015  ERR EvTestMsg            (00000000)
			//AMC, 0,         4005,           24,       2019-02-08 16:44:50, 0,           -,         TestMessage               @ 2019-02-08 16:49:36.559: EvTestMessage 2019-02-08 16:49:36.559:   0FA50018 0FA50018  ERR EvTestMsg            (00000000)

			string[] parts = line.Split(new char[]{','},  StringSplitOptions.RemoveEmptyEntries);
			if (parts?.Length < 8)
			{
				throw new BadCastException($"Parse: Split != 8 ==> MessagePlc from: {line}");
			}

			try
			{
				DefModule = AI.Lib.EnterpriseDefines.EnumConverters.ParseModuleDefinition(parts[0]);

				if (UInt32.TryParse(parts[1], out UInt32 id))
					Id = id;

				if (UInt16.TryParse(parts[2], out UInt16 mid))
					MachinePartId = mid;

				if (UInt16.TryParse(parts[3], out UInt16 msgid))
					MessageId = msgid;

				TimeStamp = DateTime.ParseExact(parts[4].Trim(), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None);

				if (UInt32.TryParse(parts[3], out UInt32 cmdId))
					ResetCmdId = cmdId;

				Parameter = parts[6].Trim();
				InfoString = parts[7].Trim();

				FindCategoryFromInfoString();
			}
			catch (Exception ex)
			{
				throw new BadCastException($"Parse: to variables ==> MessagePlc from: {line} {ex.Message}");
			}
		}

		private void FindCategoryFromInfoString()
		{
			if (InfoString.Contains(" ERR "))
				PlcMsgCategory = MessageCategory.Error;
			else if (InfoString.Contains(" WARN "))
				PlcMsgCategory = MessageCategory.Warning;
			else
				PlcMsgCategory = MessageCategory.Unknown;
		}

		public string ToRawString()
		{
			return String.Format($"{this.DefModule}, {this.Id}, {this.MachinePartId}, {this.MessageId}, " +
			$"{this.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss")}, " +
			$"{this.ResetCmdId}, {this.Parameter}, {this.InfoString}");
		}

		public MessagePlc(EvTestMessage em, ModuleDefinition modDef)
		{
			DefModule = modDef;
			//em.Msg is used, not em.Message
			Id = em.Msg.Id;
			Parameter = em.Msg.Parameter;
			ResetCmdId = em.Msg.ResetCmdId;
			TimeStamp = em.Msg.TimeStamp;
			MachinePartId = em.Msg.MachinePartId;
			MessageId = em.Msg.MessageId;
			InfoString = em.Msg.InfoString();
			FindCategoryFromInfoString();
		}

		public MessagePlc(EvtTest em, ModuleDefinition modDef)
		{
			DefModule = modDef;
			Id = 0;
			Parameter = "-";
			ResetCmdId = 0;
			TimeStamp = em.TimeEvent;
			MachinePartId = em.MachinePartId;
			MessageId = em.MessageId;
			InfoString = em.InfoString;
			FindCategoryFromInfoString();
		}

		public override string ToString()
		{
			return $"mid:{MessageId} {(Resolved?"Resolved":"Not resolved")} Db:{DbMsgCategory.ToString().Substring(0,3)} Plc:{DbMsgCategory.ToString().Substring(0,3)} mpid:{MachinePartId}";
		}
	}
}