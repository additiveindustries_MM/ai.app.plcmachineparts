﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlcMachineParts.Collections
{
	public enum TargetDatabase
	{
		Local,
        cmdb,
        cmdb_2018R21,
        cmdb_2018R22
    }

	public enum StatusMsg
	{
		Info,
		Warning,
		Error,
	}

	[Flags]
	public enum MaterialTypeUse
	{
		None = 0,
		Print = 1,
		Buildplate = 2,
		Both = 4
	}

	public enum ExportFormat
	{
		NotSet = 0,
		Cordis = 1,
		AI = 2,
		Document = 3,
	}

	public enum SettingsType
	{
		NotSet = 0,
		Default = 1,
		ECO = 2,
		NonConform = 3,
		Calibration = 4,
		Config = 5,
	}

	public enum RecordAction
	{
		NotSet,
		Edit,
		Add,
		Delete
	}

/*
	/// <summary> Defined ModuleDefinitions </summary>
	public enum ModuleDefinition : byte
	{
		///<summary> [0] Not yet has a value for Undefined. DisplayName:Not yet has a value</summary>
		Undefined = 0,
		///<summary> [1] Controls Module (only 1 possible) for Machine. DisplayName:Controls</summary>
		CTM = 1,
		///<summary> [2] Exposure Module (0-1 possible) for Machine. DisplayName:Exposure</summary>
		EXP = 2,
		///<summary> [3] Robot Module (0-1 possible) for Machine. DisplayName:Robot</summary>
		ROB = 3,
		///<summary> [10] Exchange Module (only 1 possible) for Machine. DisplayName:Exchange</summary>
		EXC = 10,
		///<summary> [20] Additive Manufacturing Core Modules (1-5 modules possible) for Machine. DisplayName:AM Core</summary>
		AMC = 20,
		///<summary> [30] Storage Modules (0-2 modules possible) for Machine. DisplayName:Storage</summary>
		STM = 30,
		///<summary> [40] Heat Treatment Modules (0-2 modules possible) for Machine. DisplayName:Heat Treatment</summary>
		HTM = 40,
		///<summary> [50] Product Removal Modules (0-2 modules possible) for Machine. DisplayName:Product Removal</summary>
		PRM = 50,
		AMF = 60,
		RCM = 90,
		HMI = 91,
		OPS = 200,
		LSR = 210,
		SYS = 255,
	}
*/


	/// <summary> See database as well!! </summary>
	public enum EcoRequirement
	{
		NotSet = 0,
		Required = 1,
		RetrofitOnFailure = 2,
		CustomerSpecificRequest = 3,
	}
	/// <summary> See database as well!! </summary>
	public enum EcoImplement
	{
		NotSet = 0,
		NotApplicable = 1,
		NotImplemented = 2,
		Implemented = 3,
		WillNotImplement = 4
	}

	public enum FilterResult
	{
		True,
		False,
		Error
	}
}
