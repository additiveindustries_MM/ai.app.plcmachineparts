﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AI.Lib.DatabaseConstants;
using PlcMachineParts.Collections;

namespace PlcMachineParts.Controls.Comboboxes
{
	class ComboModuleDefinition : ComboBox
	{
		public ModuleDefinition Selected {get => (ModuleDefinition?) SelectedItem ?? ModuleDefinition.Undefined; set => SelectedItem = value;}

		public ComboModuleDefinition()
		{
			this.DropDownStyle = ComboBoxStyle.DropDownList;
		}

		public void Fill(bool inclNotSet)
		{
			this.Items.Clear();
			foreach (ModuleDefinition item in Enum.GetValues(typeof(ModuleDefinition)))
			{
				if (((int)item == 0 && inclNotSet) || (int)item != 0)
					this.Items.Add(item);
			}
			this.SelectedIndex = 0;
		}
	}
}
