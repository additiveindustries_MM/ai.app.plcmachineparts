﻿namespace AI.App.PlcMachineParts.Controls
{
	partial class CtrlFilterMachinePart
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.chkCaseSensitive = new System.Windows.Forms.CheckBox();
            this.txtFilterPathWildcard = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkShowUnresolvedMsg = new System.Windows.Forms.CheckBox();
            this.chkShowUnresolvedPart = new System.Windows.Forms.CheckBox();
            this.chkShowCatDiff = new System.Windows.Forms.CheckBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtFilterSortedColumnWildcard = new System.Windows.Forms.TextBox();
            this.cmbSortMachineParts = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chkShowNoMessages = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chkCaseSensitive
            // 
            this.chkCaseSensitive.AutoSize = true;
            this.chkCaseSensitive.Location = new System.Drawing.Point(3, 24);
            this.chkCaseSensitive.Name = "chkCaseSensitive";
            this.chkCaseSensitive.Size = new System.Drawing.Size(94, 17);
            this.chkCaseSensitive.TabIndex = 63;
            this.chkCaseSensitive.Text = "Case sensitive";
            this.chkCaseSensitive.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkCaseSensitive.UseVisualStyleBackColor = true;
            this.chkCaseSensitive.CheckedChanged += new System.EventHandler(this.chkAny_CheckedChanged);
            this.chkCaseSensitive.Click += new System.EventHandler(this.chkAny_Click);
            // 
            // txtFilterPathWildcard
            // 
            this.txtFilterPathWildcard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterPathWildcard.Location = new System.Drawing.Point(73, 1);
            this.txtFilterPathWildcard.Name = "txtFilterPathWildcard";
            this.txtFilterPathWildcard.Size = new System.Drawing.Size(812, 20);
            this.txtFilterPathWildcard.TabIndex = 62;
            this.txtFilterPathWildcard.Text = "*";
            this.toolTip1.SetToolTip(this.txtFilterPathWildcard, "Search on Machine path name. Use wildcards *motor* or *mo?or*. \r\nPress F9 to sear" +
        "ch\r\nDouble-click to reset: *");
            this.txtFilterPathWildcard.DoubleClick += new System.EventHandler(this.txtFilterPathWildcard_DoubleClick);
            this.txtFilterPathWildcard.Leave += new System.EventHandler(this.txtFilterPathWildcard_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 61;
            this.label1.Text = "Filter path [F9]";
            // 
            // chkShowUnresolvedMsg
            // 
            this.chkShowUnresolvedMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowUnresolvedMsg.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkShowUnresolvedMsg.Location = new System.Drawing.Point(666, 21);
            this.chkShowUnresolvedMsg.Name = "chkShowUnresolvedMsg";
            this.chkShowUnresolvedMsg.Size = new System.Drawing.Size(112, 23);
            this.chkShowUnresolvedMsg.TabIndex = 63;
            this.chkShowUnresolvedMsg.Text = "Unresolved msg";
            this.chkShowUnresolvedMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.chkShowUnresolvedMsg, "Show messages that are not in the database");
            this.chkShowUnresolvedMsg.UseVisualStyleBackColor = true;
            this.chkShowUnresolvedMsg.CheckedChanged += new System.EventHandler(this.chkAny_CheckedChanged);
            this.chkShowUnresolvedMsg.Click += new System.EventHandler(this.chkAny_Click);
            // 
            // chkShowUnresolvedPart
            // 
            this.chkShowUnresolvedPart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowUnresolvedPart.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkShowUnresolvedPart.Location = new System.Drawing.Point(779, 21);
            this.chkShowUnresolvedPart.Name = "chkShowUnresolvedPart";
            this.chkShowUnresolvedPart.Size = new System.Drawing.Size(106, 23);
            this.chkShowUnresolvedPart.TabIndex = 63;
            this.chkShowUnresolvedPart.Text = "Unresolved part";
            this.chkShowUnresolvedPart.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.chkShowUnresolvedPart, "Show machineparts that are not in the database");
            this.chkShowUnresolvedPart.UseVisualStyleBackColor = true;
            this.chkShowUnresolvedPart.CheckedChanged += new System.EventHandler(this.chkAny_CheckedChanged);
            this.chkShowUnresolvedPart.Click += new System.EventHandler(this.chkAny_Click);
            // 
            // chkShowCatDiff
            // 
            this.chkShowCatDiff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowCatDiff.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkShowCatDiff.Location = new System.Drawing.Point(593, 21);
            this.chkShowCatDiff.Name = "chkShowCatDiff";
            this.chkShowCatDiff.Size = new System.Drawing.Size(72, 23);
            this.chkShowCatDiff.TabIndex = 63;
            this.chkShowCatDiff.Text = "Cat";
            this.chkShowCatDiff.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.chkShowCatDiff, "Show messages with different category");
            this.chkShowCatDiff.UseVisualStyleBackColor = true;
            this.chkShowCatDiff.CheckedChanged += new System.EventHandler(this.chkAny_CheckedChanged);
            this.chkShowCatDiff.Click += new System.EventHandler(this.chkAny_Click);
            // 
            // txtFilterSortedColumnWildcard
            // 
            this.txtFilterSortedColumnWildcard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilterSortedColumnWildcard.Location = new System.Drawing.Point(339, 22);
            this.txtFilterSortedColumnWildcard.Name = "txtFilterSortedColumnWildcard";
            this.txtFilterSortedColumnWildcard.Size = new System.Drawing.Size(170, 20);
            this.txtFilterSortedColumnWildcard.TabIndex = 80;
            this.txtFilterSortedColumnWildcard.Text = "*";
            this.toolTip1.SetToolTip(this.txtFilterSortedColumnWildcard, "Search on Machine path name. Use wildcards *motor* or *mo?or*. \r\nPress F9 to sear" +
        "ch\r\nDouble-click to reset: *");
            this.txtFilterSortedColumnWildcard.DoubleClick += new System.EventHandler(this.txtFilterSortedColumn_DoubleClick);
            this.txtFilterSortedColumnWildcard.Leave += new System.EventHandler(this.txtFilterSortedColumn_Leave);
            // 
            // cmbSortMachineParts
            // 
            this.cmbSortMachineParts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSortMachineParts.FormattingEnabled = true;
            this.cmbSortMachineParts.Location = new System.Drawing.Point(154, 22);
            this.cmbSortMachineParts.Name = "cmbSortMachineParts";
            this.cmbSortMachineParts.Size = new System.Drawing.Size(128, 21);
            this.cmbSortMachineParts.TabIndex = 79;
            this.cmbSortMachineParts.SelectedIndexChanged += new System.EventHandler(this.cmbSortMachineParts_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(127, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Sort";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(288, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 61;
            this.label3.Text = "Filter [F9]";
            // 
            // chkSuppressNoMessages
            // 
            this.chkShowNoMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkShowNoMessages.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkShowNoMessages.Location = new System.Drawing.Point(511, 21);
            this.chkShowNoMessages.Name = "chkShowNoMessages";
            this.chkShowNoMessages.Size = new System.Drawing.Size(79, 23);
            this.chkShowNoMessages.TabIndex = 63;
            this.chkShowNoMessages.Text = "Hide no msg";
            this.chkShowNoMessages.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.chkShowNoMessages, "Suppress machineparts with no messages");
            this.chkShowNoMessages.UseVisualStyleBackColor = true;
            this.chkShowNoMessages.CheckedChanged += new System.EventHandler(this.chkAny_CheckedChanged);
            this.chkShowNoMessages.Click += new System.EventHandler(this.chkAny_Click);
            // 
            // CtrlFilterMachinePart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtFilterSortedColumnWildcard);
            this.Controls.Add(this.cmbSortMachineParts);
            this.Controls.Add(this.chkShowUnresolvedPart);
            this.Controls.Add(this.chkShowNoMessages);
            this.Controls.Add(this.chkShowCatDiff);
            this.Controls.Add(this.chkShowUnresolvedMsg);
            this.Controls.Add(this.txtFilterPathWildcard);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chkCaseSensitive);
            this.Name = "CtrlFilterMachinePart";
            this.Size = new System.Drawing.Size(887, 44);
            this.Load += new System.EventHandler(this.CtrlFilterMachinePart_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckBox chkCaseSensitive;
		private System.Windows.Forms.TextBox txtFilterPathWildcard;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox chkShowUnresolvedMsg;
		private System.Windows.Forms.CheckBox chkShowUnresolvedPart;
		private System.Windows.Forms.CheckBox chkShowCatDiff;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox cmbSortMachineParts;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFilterSortedColumnWildcard;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chkShowNoMessages;
    }
}
