﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AI.Lib.Global;
using PlcMachineParts;
using PlcMachineParts.CiFile;

namespace AI.App.PlcMachineParts.Controls
{
    public interface IFilterMachinePart
    {
        bool FilterOnPath(MachinePartPlc x);
        bool FilterOnSortedColumn(MachinePartPlc x);
        SortMachinePart SelectedSort { get; set; }
        event EventHandler<EventArgs> FilterParametersChanged;
	}

	public partial class CtrlFilterMachinePart : UserControl, IFilterMachinePart
	{
		public event EventHandler<EventArgs> FilterParametersChanged;

        public SortMachinePart SelectedSort
        {
            get => (cmbSortMachineParts.SelectedIndex > -1) ? (SortMachinePart) cmbSortMachineParts.SelectedItem : SortMachinePart.NoSort;
            set => cmbSortMachineParts.SelectedItem = (object)value;
        }

		public bool FilterWildcardCaseSensitive
		{
			get => chkCaseSensitive.Checked;
			set => chkCaseSensitive.Checked = value;
		}

		public bool FilterShowUnresolvedMsg
		{
			get => chkShowUnresolvedMsg.Checked;
			set => chkShowUnresolvedMsg.Checked = value;
		}

        public bool FilterShowUnresolvedPart
        {
            get => chkShowUnresolvedPart.Checked;
            set => chkShowUnresolvedPart.Checked = value;
        }
        public bool FilterShowNoMessages
        {
            get => chkShowNoMessages.Checked;
            set => chkShowNoMessages.Checked = value;
        }

		public bool FilterShowCatDiff
		{
			get => chkShowCatDiff.Checked;
			set => chkShowCatDiff.Checked = value;
		}

		public string FilterPathWildcard
		{
			get => txtFilterPathWildcard.Text;
			set => txtFilterPathWildcard.Text = value;
		}


        public string FilterSortedColumnWildcard
        {
            get => txtFilterSortedColumnWildcard.Text;
            set => txtFilterSortedColumnWildcard.Text = value;
        }

		private int _difCat = 0;
		public int DifCat
		{
			get => _difCat;
			set { _difCat = value; chkShowCatDiff.Text = $"Cat ({_difCat})"; }
		}

		private int _difMsgUnresolved = 0;
		public int DifMsgUnresolved
		{
			get => _difMsgUnresolved;
			set { _difMsgUnresolved = value;  chkShowUnresolvedMsg.Text = $"Unrslvd Msg ({_difMsgUnresolved})";}
		}

		private int _difPartUnresolved = 0;
		public int DifPartUnresolved
		{
			get => _difPartUnresolved;
			set { _difPartUnresolved = value;  chkShowUnresolvedPart.Text = $"Unrslvd Prts ({_difPartUnresolved})";}
		}

		public CtrlFilterMachinePart()
		{
			InitializeComponent();
		}

		public bool FilterOnPath(MachinePartPlc x)
		{
			return 
				x.MachinePath.WildcardMatch(FilterPathWildcard, !FilterWildcardCaseSensitive) &&
				(x.CountMessagesUnResolved > 0 || !FilterShowUnresolvedMsg) &&
				(x.MessagesCategoryDiff > 0 || !FilterShowCatDiff) &&
                (!x.HasMachinePart || !FilterShowUnresolvedPart) &&

                (x.CountMessages != 0 || !FilterShowNoMessages)

				;
		}

        public bool FilterOnSortedColumn(MachinePartPlc x)
        {
            switch (this.SelectedSort)
            {
                case SortMachinePart.MachinePartId:
					return x.MachinePartId.ToString().WildcardMatch(FilterSortedColumnWildcard, true);
				case SortMachinePart.MachinePartDefinitionId:
                    return x.MachinePartDefinitionId.ToString().WildcardMatch(FilterSortedColumnWildcard, true);
                case SortMachinePart.MachinePartName:
                    return x.MachinePartName.WildcardMatch(FilterSortedColumnWildcard, true);
                case SortMachinePart.MachinePath:
                    return x.MachinePath.WildcardMatch(FilterSortedColumnWildcard, true);
                case SortMachinePart.AssemblyCode:
                    return x.AssemblyCode.ToString().WildcardMatch(FilterSortedColumnWildcard, true);
                case SortMachinePart.Parent:
                    return x.MachinePartParentId.ToString().WildcardMatch(FilterSortedColumnWildcard, true);
                case SortMachinePart.NoSort:
                default:
                    return true;
            }
        }


		private void FireSelectedIndexChanged()
		{
			EventHandler<EventArgs> handler = FilterParametersChanged;
			handler?.Invoke(this, new EventArgs());
		}
		private void chkAny_CheckedChanged(object sender, EventArgs e)
		{}

		private void chkAny_Click(object sender, EventArgs e)
        {
			FireSelectedIndexChanged();
		}

        private void CtrlFilterMachinePart_Load(object sender, EventArgs e)
        {
            cmbSortMachineParts.DataSource = Enum.GetValues(typeof(SortMachinePart));
            cmbSortMachineParts.SelectedIndex = 0;
        }

        private void cmbSortMachineParts_SelectedIndexChanged(object sender, EventArgs e)
        {
            FireSelectedIndexChanged();
		}

        private void txtFilterPathWildcard_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtFilterPathWildcard.Text))
                txtFilterPathWildcard.Text = "*";
            FireSelectedIndexChanged();
        }

        private void txtFilterPathWildcard_DoubleClick(object sender, EventArgs e)
        {
            txtFilterPathWildcard.Text = "*";
            FireSelectedIndexChanged();
        }

		private void txtFilterSortedColumn_DoubleClick(object sender, EventArgs e)
        {
            txtFilterSortedColumnWildcard.Text = "*";
            FireSelectedIndexChanged();
		}

		private void txtFilterSortedColumn_Leave(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtFilterSortedColumnWildcard.Text))
                txtFilterSortedColumnWildcard.Text = "*";
            FireSelectedIndexChanged();
        }
    }
}
