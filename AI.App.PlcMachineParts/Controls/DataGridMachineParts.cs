﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AI.App.PlcMachineParts.Controls;
using PlcMachineParts.CiFile;

namespace PlcMachineParts.Controls
{
	public class DataGridViewMachineParts : DataGridView
	{
        private List<MachinePartPlc> __allMachineParts = new List<MachinePartPlc>();
        public long CountAllMachineParts => __allMachineParts?.Count() ?? -1;
        public long CountAllMessages => __allMachineParts?.Sum(list => list.Messages.Count) ?? -1;

        public List<MachinePartPlc> AllMachineParts
        {
            get => __allMachineParts;
            set => __allMachineParts = value;
        }

        public IFilterMachinePart FilterMachinePart { get; set; }


		private ContextMenuStrip contextMenuStrip1;
		private System.ComponentModel.IContainer components;
		private ToolStripMenuItem toolStripMenuItemCopyToClipboard;

		public DataGridViewMachineParts()
		{
			InitializeComponent();
			this.Font = new Font("Arial Narrow", 8, FontStyle.Regular);

			this.DataBindingComplete += DataGridSettingsCalibration_DataBindingComplete;
			this.CellFormatting += DataGridSettingsCalibration_CellFormatting;
            EnableHeadersVisualStyles = false;
			toolStripMenuItemCopyToClipboard.Click += ToolStripMenuItemCopyToClipboard_Click;
            RowHeadersWidth = 20;
        }

		private void DataGridSettingsCalibration_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			// Once after each databind. Set columns visibiility, width, header
			this.SetColumnsVisible(false); // Set all invisible, next lines set some back again
			int colIdx = 0;
			this.SetColumn(nameof(MachinePartPlc.MachinePartId),             "MPid",     60, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.HasMachinePart),             "HasMp",     50, colIdx++);

			//this.SetColumn(nameof(MachinePartPlc.HasMachinePartParent),             "HasParent",     50, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.MachinePartParentId),             "Parent",     60, colIdx++);

			this.SetColumn(nameof(MachinePartPlc.HasMachinePartDefinition),             "HasDef",     50, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.MachinePartDefinitionId),             "MpDefId",     50, colIdx++);

			this.SetColumn(nameof(MachinePartPlc.MachinePartName),    "Name",    100, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.CountMessages),    "#msg",    50, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.CountMessagesResolved),    "#rslvd",    50, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.CountMessagesUnResolved),    "#unrslvd",    50, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.MessagesCategoryDiff),    "#Cat",    50, colIdx++);

			this.SetColumn(nameof(MachinePartPlc.MessageOffset),    "Offset",    40, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.AssemblyCode),    "Assy",    60, colIdx++);

			this.SetColumn(nameof(MachinePartPlc.ModDefDbMachinePartPlcTxt       ),    "PLC",    40, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.ModDefDbMachinePartDefinitionTxt),    "Def",    40, colIdx++);

			this.SetColumn(nameof(MachinePartPlc.DescrPart),    "DscrPart",    50, colIdx++);
			this.SetColumn(nameof(MachinePartPlc.DescrDef),    "DscrDef",    50, colIdx++);

			this.SetColumn(nameof(MachinePartPlc.MachinePath),    "Path",    123, colIdx++);
			this.SetColumnAutoSizeMode(nameof(MachinePartPlc.MachinePath), DataGridViewAutoSizeColumnMode.Fill);
		}

		private void DataGridSettingsCalibration_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (Rows.Count <= 0 || Columns.Count <= 0 || e.ColumnIndex < 0 || e.ColumnIndex > Columns.Count || e.RowIndex < 0 || Rows[e.RowIndex] == null)
				return;

			if (Rows[e.RowIndex].DataBoundItem != null && Rows[e.RowIndex].DataBoundItem is MachinePartPlc sel)
			{
				if (this.Columns[e.ColumnIndex].Name.Equals(nameof(MachinePartPlc.CountMessagesUnResolved)))
				{
					this.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = sel.CountMessagesUnResolved == 0 ? Color.White:Color.LightSalmon;
				}
				else if (this.Columns[e.ColumnIndex].Name.Equals(nameof(MachinePartPlc.HasMachinePart)))
				{
					this.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = sel.HasMachinePart? Color.White:Color.LightSalmon;
				}
				else if (this.Columns[e.ColumnIndex].Name.Equals(nameof(MachinePartPlc.HasMachinePartDefinition)))
				{
					this.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = sel.HasMachinePartDefinition ? Color.White:Color.LightSalmon;
				}
				else if (this.Columns[e.ColumnIndex].Name.Equals(nameof(MachinePartPlc.MachinePartParentId)))
				{
					// Paint red when no parent and there is more then one '/' in the path
					this.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = sel.MachinePartParentId == 0 && sel.MachinePath.Count(ch => ch == '/') > 1 ?Color.LightSalmon :  Color.White;
				}
				else if (this.Columns[e.ColumnIndex].Name.Equals(nameof(MachinePartPlc.MessagesCategoryDiff)))
				{
					this.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = sel.MessagesCategoryDiff == 0 ? Color.White : Color.LightSalmon;
				}
			}
		}

		public void SetDataSourceMachinePart(bool clear = false)
		{
            if (FilterMachinePart == null)
            {
                throw new NullReferenceException("FilterMachinePart == null");
            }

			if (clear)
			{
				DataSource = null;
			}
			else
			{
				switch (FilterMachinePart.SelectedSort)
				{
					case SortMachinePart.MachinePartId:
						DataSource = AllMachineParts.Where(x => FilterMachinePart.FilterOnPath(x) && FilterMachinePart.FilterOnSortedColumn(x)).OrderBy(x => x.MachinePartId).ToList();
						break;
					case SortMachinePart.MachinePartDefinitionId:
						DataSource = AllMachineParts.Where(x => FilterMachinePart.FilterOnPath(x) && FilterMachinePart.FilterOnSortedColumn(x)).OrderBy(x => x.MachinePartDefinitionId).ToList();
						break;
					case SortMachinePart.MachinePartName:
						DataSource = AllMachineParts.Where(x => FilterMachinePart.FilterOnPath(x) && FilterMachinePart.FilterOnSortedColumn(x)).OrderBy(x => x.MachinePartName).ToList();
						break;
					case SortMachinePart.AssemblyCode:
						DataSource = AllMachineParts.Where(x => FilterMachinePart.FilterOnPath(x) && FilterMachinePart.FilterOnSortedColumn(x)).OrderBy(x => x.AssemblyCode).ThenBy(x => x.MessageOffset).ToList();
						break;
					case SortMachinePart.Parent:
						DataSource = AllMachineParts.Where(x => FilterMachinePart.FilterOnPath(x) && FilterMachinePart.FilterOnSortedColumn(x)).OrderBy(x => x.MachinePartParentId).ToList();
						break;
                    case SortMachinePart.MachinePath:
                        DataSource = AllMachineParts.Where(x => FilterMachinePart.FilterOnPath(x) && FilterMachinePart.FilterOnSortedColumn(x)).OrderBy(x => x.MachinePath).ToList();
						break;
                    case SortMachinePart.NoSort:
					default:
						DataSource = AllMachineParts.Where(x => FilterMachinePart.FilterOnPath(x) && FilterMachinePart.FilterOnSortedColumn(x)).ToList();
						break;
                }

                if (this.Columns.Count > 0)
                {
                    int sortedCol = ColNameToSortToColIndex(FilterMachinePart.SelectedSort);
					foreach (DataGridViewColumn column in this.Columns)
                    {
                        SortMachinePart sort = ColNameToSort(column.Name);

                        if (column.Index == sortedCol)
                        {
                            column.HeaderCell.Style.BackColor = SystemColors.ControlDark;
                            column.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Bold);
						}
						else if (sort != SortMachinePart.NoSort)
                        {
                            column.HeaderCell.Style.BackColor = SystemColors.ControlLightLight;
							column.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Bold);
                        }
						else
                        {
                            column.HeaderCell.Style.BackColor = SystemColors.Control;
                            column.HeaderCell.Style.Font = new Font(this.Font, FontStyle.Regular);
                        }
						//column.HeaderCell.Style.BackColor = (column.Index == sortedCol) ? SystemColors.ControlDark : SystemColors.Control;
						column.HeaderCell.SortGlyphDirection = (column.Index == sortedCol) ? SortOrder.Ascending : SortOrder.None;
					}
                }
			}
		}

		private void ToolStripMenuItemCopyToClipboard_Click(object sender, EventArgs e)
		{
/*
			if (((ToolStripMenuItem)sender).Tag != null && ((ToolStripMenuItem)sender).Tag is string s)
				Clipboard.SetText(s);
			else
				Clipboard.SetText("paste error");
*/

			StringBuilder sb = new StringBuilder();
			foreach (DataGridViewCell cell in SelectedCells)
			{
				sb.AppendLine(cell.Value?.ToString()??"NULL");
			}
			Clipboard.SetText(sb.ToString());
		}

		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemCopyToClipboard});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(170, 26);
            // 
            // toolStripMenuItemCopyToClipboard
            // 
            this.toolStripMenuItemCopyToClipboard.Name = "toolStripMenuItemCopyToClipboard";
            this.toolStripMenuItemCopyToClipboard.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItemCopyToClipboard.Text = "Copy to clipboard";
            // 
            // DataGridViewMachineParts
            // 
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridViewMachineParts_ColumnHeaderMouseClick);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

		}


        public int ColNameToSortToColIndex(SortMachinePart srt)
        {
            if (this.Columns.Count == 0)
                return - 1;

            switch (srt)
            {
                case SortMachinePart.MachinePartId:
                    return this.Columns[nameof(MachinePartPlc.MachinePartId)].Index;
				case SortMachinePart.MachinePartDefinitionId:
                    return this.Columns[nameof(MachinePartPlc.MachinePartDefinitionId)].Index;
                case SortMachinePart.MachinePartName:
                    return this.Columns[nameof(MachinePartPlc.MachinePartName)].Index;
                case SortMachinePart.MachinePath:
                    return this.Columns[nameof(MachinePartPlc.MachinePath)].Index;
                case SortMachinePart.AssemblyCode:
                    return this.Columns[nameof(MachinePartPlc.AssemblyCode)].Index;
                case SortMachinePart.Parent:
                    return this.Columns[nameof(MachinePartPlc.MachinePartParentId)].Index;
				case SortMachinePart.NoSort:
                    return -1;
                default:
                    throw new ArgumentOutOfRangeException(nameof(srt), srt, null);
            }
        }

		public static SortMachinePart ColNameToSort(string colName)
        {
            if (String.IsNullOrEmpty(colName))
                return SortMachinePart.NoSort;

            switch (colName)
            {
                case nameof(MachinePartPlc.MachinePartId):
                    return SortMachinePart.MachinePartId;
                case nameof(MachinePartPlc.MachinePartName):
                    return SortMachinePart.MachinePartName;
                case nameof(MachinePartPlc.MachinePartDefinitionId):
                    return SortMachinePart.MachinePartDefinitionId;
                case nameof(MachinePartPlc.MachinePartParentId):
                    return SortMachinePart.Parent;
                case nameof(MachinePartPlc.AssemblyCode):
                    return SortMachinePart.AssemblyCode;
                case nameof(MachinePartPlc.MachinePath):
                    return SortMachinePart.MachinePath;
                default:
                    return SortMachinePart.NoSort;
            }
        }

        private void DataGridViewMachineParts_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Debug.WriteLine($"clicked column: {e.ColumnIndex} = {this.Columns[e.ColumnIndex].Name} not handled");
            FilterMachinePart.SelectedSort = ColNameToSort(this.Columns[e.ColumnIndex].Name);
		}
	}
}
