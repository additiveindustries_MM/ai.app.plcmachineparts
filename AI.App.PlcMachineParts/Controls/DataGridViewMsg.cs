﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PlcMachineParts.CiFile;

namespace PlcMachineParts.Controls
{
	public class DataGridViewMsg : DataGridView
	{
		public DataGridViewMsg()
        {
            InitializeComponent();
            RowHeadersWidth = 20;
			this.DataBindingComplete += DataGridSettingsCalibration_DataBindingComplete;
			this.CellFormatting += DataGridSettingsCalibration_CellFormatting;
		}

		private void DataGridSettingsCalibration_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
		{
			// Once after each databind. Set columns visibiility, width, header
			this.SetColumnsVisible(false); // Set all invisible, next lines set some back again
			int colIdx = 0;
			this.SetColumn(nameof(MessagePlc.MachinePartId),             "MPid",     40, colIdx++);
			this.SetColumn(nameof(MessagePlc.MessageId),    "Mid",    70, colIdx++);
			this.SetColumn(nameof(MessagePlc.PlcMsgCategory),    "PlcCat",    70, colIdx++);
			this.SetColumn(nameof(MessagePlc.DbMsgCategory),    "DbCat",    70, colIdx++);
			this.SetColumn(nameof(MessagePlc.Descr),    "Descr",    40, colIdx++);
			this.SetColumn(nameof(MessagePlc.MsgShortName),    "Short",    100, colIdx++);
			this.SetColumn(nameof(MessagePlc.MsgFullText),    "Full",    100, colIdx++);
			this.SetColumnAutoSizeMode(nameof(MessagePlc.MsgFullText), DataGridViewAutoSizeColumnMode.Fill);
		}

		private void DataGridSettingsCalibration_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (Rows.Count <= 0 || Columns.Count <= 0 || e.ColumnIndex < 0 || e.ColumnIndex > Columns.Count || e.RowIndex < 0 || Rows[e.RowIndex] == null)
				return;

			if (Rows[e.RowIndex].DataBoundItem != null && Rows[e.RowIndex].DataBoundItem is MessagePlc sel)
			{
				if (!sel.Resolved)
				{
					this.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightSalmon;
				}
				else if
				(
					this.Columns[e.ColumnIndex].Name.Equals(nameof(MessagePlc.PlcMsgCategory)) ||
					this.Columns[e.ColumnIndex].Name.Equals(nameof(MessagePlc.DbMsgCategory))
				)
				{
					this.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = sel.Resolved && sel.DbMsgCategory == sel.PlcMsgCategory ? Color.White:Color.LightSalmon;
				}
			}
		}

        private void ToolStripMenuItemCopyToClipboard_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (DataGridViewCell cell in SelectedCells)
            {
                sb.AppendLine(cell.Value?.ToString() ?? "NULL");
            }
            Clipboard.SetText(sb.ToString());
        }

		private ContextMenuStrip contextMenuStrip1;
        private System.ComponentModel.IContainer components;
        private ToolStripMenuItem toolStripMenuItemCopyToClipboard;

		private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
                this.toolStripMenuItemCopyToClipboard});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(170, 26);
            // 
			// toolStripMenuItemCopyToClipboard
			// 
			this.toolStripMenuItemCopyToClipboard.Name = "toolStripMenuItemCopyToClipboard";
            this.toolStripMenuItemCopyToClipboard.Size = new System.Drawing.Size(169, 22);
            this.toolStripMenuItemCopyToClipboard.Text = "Copy to clipboard";
            this.toolStripMenuItemCopyToClipboard.Click += ToolStripMenuItemCopyToClipboard_Click;
            // 
            // DataGridViewMachineParts
            // 
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }
	}
}
