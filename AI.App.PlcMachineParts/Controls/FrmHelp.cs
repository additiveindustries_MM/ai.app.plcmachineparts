﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AI.App.PlcMachineParts.DataModel;

namespace AI.App.PlcMachineParts.Controls
{
	public partial class FrmHelp : Form
	{
		public FrmHelp()
		{
			InitializeComponent();
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void FrmHelp_Load(object sender, EventArgs e)
		{
			lblProgram.Text = $"Application: {Assembly.GetExecutingAssembly().GetName().Name}";;
			lblVersion.Text = $"Version: {Assembly.GetExecutingAssembly().GetName().Version}";
			//GetDbConnection();
		}
/*
		private void xGetDbConnection()
		{
			ConnectionStringSettings dc = System.Configuration.ConfigurationManager.ConnectionStrings[SettingsDbEntities.ConnectionName];
			if (dc != null)
			{
				var entityBuilder = new EntityConnectionStringBuilder(dc.ConnectionString); // Full EF6 string, contains a lot of extra info
				SqlConnectionStringBuilder decoder = new SqlConnectionStringBuilder(entityBuilder.ProviderConnectionString);
				lblDatabase.Text = $"Connectionstring: server={decoder.DataSource}; user={decoder.UserID}; password=***; database={decoder.InitialCatalog}";
			}
			else
			{
				lblDatabase.Text = $"Connectionstring: unknown";
			}
		}
*/
		private void linkDownloadConnector_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			linkDownloadConnector.LinkVisited = true;
			System.Diagnostics.Process.Start(linkDownloadConnector.Text);
		}
	}
}
