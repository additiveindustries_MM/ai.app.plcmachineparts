﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using AI.Lib.Global;
using PlcMachineParts;

namespace AI.App.PlcMachineParts.Controls
{
	public partial class SplashForm : Form
	{
		//Delegate for cross thread call to close
		private delegate void CloseDelegate();
		//The type of form to be displayed as the splash screen.
		private static SplashForm _splashForm;

		private static string _msg = null;
		public SplashForm()
		{
			InitializeComponent();
		}
		public static void ShowSplashScreen(string msg = null)
		{
			// Make sure it is only launched once.

			if (_splashForm != null)
				return;

			_msg = msg;

			Thread thread = new Thread(new ThreadStart(SplashForm.ShowForm));
			thread.IsBackground = true;
			thread.SetApartmentState(ApartmentState.STA);
			thread.Start();           
		}

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		private static extern IntPtr FindWindow(string strClassName, string strWindowName);

		[DllImport("user32.dll")]
		private static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

		private struct Rect
		{
			public int Left { get; set; }
			public int Top { get; set; }
			public int Right { get; set; }
			public int Bottom { get; set; }
		}
		private static Rectangle ThisAppLocation()
		{
			Process[] processes = Process.GetProcessesByName(System.Diagnostics.Process.GetCurrentProcess().ProcessName);
			Process lol = processes[0];
			IntPtr ptr = lol.MainWindowHandle;
			Rect rect = new Rect();
			GetWindowRect(ptr, ref rect);
			return new Rectangle(rect.Left, rect.Top, rect.Right - rect.Left, rect.Bottom - rect.Top);
		}

		private static void ShowForm()
		{
			CloseForm();
			_splashForm = new SplashForm();
			if (_msg != null) // if an aleternative message was passed, that might need more space
			{
				// If the text needs more lines then make the size of the whole form bigger. (the textbox is anchored so follows) 
				_splashForm.txtMessage.Text = _msg;
				List<string> list = _splashForm.txtMessage.SplitLines();
				Debug.WriteLine($"Items: {String.Join(", ", list)}");
				int lines = _splashForm.txtMessage.SplitLines().Count; // txtbox is achored, grows
				if (lines > 1)
					_splashForm.Height += MathFuncs.Round(_splashForm.txtMessage.Height * 1.0 * (lines-1));
			}
			_splashForm.StartPosition = FormStartPosition.Manual;
			Rectangle r = ThisAppLocation();
			Point p = r.Location;
			p.Offset((r.Width - _splashForm.Width)/2, (r.Height - _splashForm.Width)/2); // middle of splash to middle of the main form
			_splashForm.Location = p;
			Application.Run(_splashForm);
		}

		public static void CloseForm()
		{
			try
			{
				_splashForm?.Invoke(new CloseDelegate(SplashForm.CloseFormInternal));
			}
			catch (Exception)
			{}
			_splashForm = null;
			_msg = null;
		}

		private static void CloseFormInternal()
		{
			_splashForm?.Close();
			_splashForm = null;
		}
	}
}
