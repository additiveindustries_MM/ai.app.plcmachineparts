﻿using System;
using System.Collections.Generic;
using System.Linq;
using AI.App.PlcMachineParts.DataModel;
using AI.Lib.DatabaseConstants.MachineParts;
using AI.Lib.EnterpriseDefines.MachineInterface.Messages;
using AI.Lib.Global;
using PlcMachineParts.Collections;

namespace PlcMachineParts.Data
{
	public class ApplicationData
	{
		public string LatestReadSource { get; set; }
		public long __freeMachinePartDefinitionId = -1;
		public long FreeMachinePartDefinitionId
		{
			get
			{
				if (__freeMachinePartDefinitionId < 0)
				{
                    SettingsDbEntities _context = new SettingsDbEntities();
					__freeMachinePartDefinitionId = _context.ec_machinepartdefinitions.Max(p => p.MACHINEPARTDEFINITION_ID) + 1;
				}
				return __freeMachinePartDefinitionId;
			}
			set { __freeMachinePartDefinitionId = value; }
		}

		public ApplicationData()
		{

			Log.Message(Severity.MESSAGE, "ApplicationData initialized");
		}
		~ApplicationData()
		{
			AI.App.PlcMachineParts.Properties.Settings.Default.Save();
		}

        public bool Reset { get; set; }

		#region MachinePartDefinitions
		private List<MachinePart> __machineParts = null;
		public List<MachinePart> MachineParts
		{
			get
			{
				if (__machineParts == null)
					__machineParts = AI.Lib.DatabaseConstants.CommandInterface.GetMachineParts();
				return __machineParts;
			}
		}


		#endregion
	}
}
