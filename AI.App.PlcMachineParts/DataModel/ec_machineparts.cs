//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AI.App.PlcMachineParts.DataModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ec_machineparts
    {
        public long MODULEDEF_ID { get; set; }
        public int MACHINEPART_ID { get; set; }
        public byte PLC_MODULEDEF_ID { get; set; }
        public int PARENTMACHINEPART_ID { get; set; }
        public int MACHINEPARTDEFINITION_ID { get; set; }
        public int ASSEMBLYCODE { get; set; }
        public int MSGOFFSET { get; set; }
        public string DESCR { get; set; }
        public string DISPLAYNAME_OVERRIDE { get; set; }
    }
}
