﻿using PlcMachineParts.Collections;
using PlcMachineParts.Controls.Comboboxes;
using PlcMachineParts.Controls;

namespace PlcMachineParts
{
	partial class FrmMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.numGotoMachinePartId = new System.Windows.Forms.NumericUpDown();
            this.grpPlcConnection = new System.Windows.Forms.GroupBox();
            this.cmbAdsAddress = new System.Windows.Forms.ComboBox();
            this.lblAdsAddress = new System.Windows.Forms.Label();
            this.chkPlcConnection = new System.Windows.Forms.CheckBox();
            this.lblPlcState = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboModuleDefinition1 = new PlcMachineParts.Controls.Comboboxes.ComboModuleDefinition();
            this.pnlPlcRelated = new System.Windows.Forms.Panel();
            this.btnRetrieveAllMessages = new System.Windows.Forms.Button();
            this.btnRetrieveMachinepartRepo = new System.Windows.Forms.Button();
            this.lblModuleDefPlc = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripItemCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusResolve = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainerTables = new System.Windows.Forms.SplitContainer();
            this.dataGridViewMachParts = new PlcMachineParts.Controls.DataGridViewMachineParts();
            this.dataGridViewMsg = new PlcMachineParts.Controls.DataGridViewMsg();
            this.splitContainerMain = new System.Windows.Forms.SplitContainer();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLoopupPlcMsgInDatabase = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.numGotoMachinePartDefId = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblLatestDefId = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonReadFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonWriteFile = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonHelp = new System.Windows.Forms.ToolStripButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ctrlFilterMachinePart1 = new AI.App.PlcMachineParts.Controls.CtrlFilterMachinePart();
            this.cmachinepartsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bplcsettingsvaluesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.toolStripButtonLogFile = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.numGotoMachinePartId)).BeginInit();
            this.grpPlcConnection.SuspendLayout();
            this.pnlPlcRelated.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTables)).BeginInit();
            this.splitContainerTables.Panel1.SuspendLayout();
            this.splitContainerTables.Panel2.SuspendLayout();
            this.splitContainerTables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMachParts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMsg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).BeginInit();
            this.splitContainerMain.Panel1.SuspendLayout();
            this.splitContainerMain.Panel2.SuspendLayout();
            this.splitContainerMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGotoMachinePartDefId)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmachinepartsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bplcsettingsvaluesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // numGotoMachinePartId
            // 
            this.numGotoMachinePartId.Location = new System.Drawing.Point(5, 28);
            this.numGotoMachinePartId.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numGotoMachinePartId.Name = "numGotoMachinePartId";
            this.numGotoMachinePartId.Size = new System.Drawing.Size(69, 20);
            this.numGotoMachinePartId.TabIndex = 5;
            this.numGotoMachinePartId.Value = new decimal(new int[] {
            1001,
            0,
            0,
            0});
            // 
            // grpPlcConnection
            // 
            this.grpPlcConnection.Controls.Add(this.cmbAdsAddress);
            this.grpPlcConnection.Controls.Add(this.lblAdsAddress);
            this.grpPlcConnection.Controls.Add(this.chkPlcConnection);
            this.grpPlcConnection.Controls.Add(this.lblPlcState);
            this.grpPlcConnection.Controls.Add(this.label6);
            this.grpPlcConnection.Controls.Add(this.comboModuleDefinition1);
            this.grpPlcConnection.Location = new System.Drawing.Point(5, 3);
            this.grpPlcConnection.Name = "grpPlcConnection";
            this.grpPlcConnection.Size = new System.Drawing.Size(357, 43);
            this.grpPlcConnection.TabIndex = 62;
            this.grpPlcConnection.TabStop = false;
            this.grpPlcConnection.Text = "PLC connection";
            // 
            // cmbAdsAddress
            // 
            this.cmbAdsAddress.FormattingEnabled = true;
            this.cmbAdsAddress.Location = new System.Drawing.Point(32, 16);
            this.cmbAdsAddress.Name = "cmbAdsAddress";
            this.cmbAdsAddress.Size = new System.Drawing.Size(109, 21);
            this.cmbAdsAddress.TabIndex = 52;
            // 
            // lblAdsAddress
            // 
            this.lblAdsAddress.AutoSize = true;
            this.lblAdsAddress.Location = new System.Drawing.Point(2, 19);
            this.lblAdsAddress.Name = "lblAdsAddress";
            this.lblAdsAddress.Size = new System.Drawing.Size(32, 13);
            this.lblAdsAddress.TabIndex = 51;
            this.lblAdsAddress.Text = "ADS:";
            // 
            // chkPlcConnection
            // 
            this.chkPlcConnection.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkPlcConnection.AutoCheck = false;
            this.chkPlcConnection.Location = new System.Drawing.Point(234, 12);
            this.chkPlcConnection.Name = "chkPlcConnection";
            this.chkPlcConnection.Size = new System.Drawing.Size(57, 25);
            this.chkPlcConnection.TabIndex = 54;
            this.chkPlcConnection.Text = "Connect";
            this.chkPlcConnection.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkPlcConnection.UseVisualStyleBackColor = true;
            this.chkPlcConnection.CheckedChanged += new System.EventHandler(this.chkPlcConnection_CheckedChanged);
            this.chkPlcConnection.Click += new System.EventHandler(this.chkPlcConnection_Click);
            // 
            // lblPlcState
            // 
            this.lblPlcState.AutoSize = true;
            this.lblPlcState.Location = new System.Drawing.Point(295, 19);
            this.lblPlcState.Name = "lblPlcState";
            this.lblPlcState.Size = new System.Drawing.Size(16, 13);
            this.lblPlcState.TabIndex = 53;
            this.lblPlcState.Text = "...";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(139, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 50;
            this.label6.Text = ":851";
            // 
            // comboModuleDefinition1
            // 
            this.comboModuleDefinition1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboModuleDefinition1.FormattingEnabled = true;
            this.comboModuleDefinition1.Location = new System.Drawing.Point(167, 14);
            this.comboModuleDefinition1.Name = "comboModuleDefinition1";
            this.comboModuleDefinition1.Selected = AI.Lib.DatabaseConstants.ModuleDefinition.Undefined;
            this.comboModuleDefinition1.Size = new System.Drawing.Size(63, 21);
            this.comboModuleDefinition1.TabIndex = 6;
            // 
            // pnlPlcRelated
            // 
            this.pnlPlcRelated.Controls.Add(this.btnRetrieveAllMessages);
            this.pnlPlcRelated.Controls.Add(this.btnRetrieveMachinepartRepo);
            this.pnlPlcRelated.Enabled = false;
            this.pnlPlcRelated.Location = new System.Drawing.Point(10, 48);
            this.pnlPlcRelated.Name = "pnlPlcRelated";
            this.pnlPlcRelated.Size = new System.Drawing.Size(352, 32);
            this.pnlPlcRelated.TabIndex = 63;
            // 
            // btnRetrieveAllMessages
            // 
            this.btnRetrieveAllMessages.Location = new System.Drawing.Point(176, 2);
            this.btnRetrieveAllMessages.Name = "btnRetrieveAllMessages";
            this.btnRetrieveAllMessages.Size = new System.Drawing.Size(172, 28);
            this.btnRetrieveAllMessages.TabIndex = 50;
            this.btnRetrieveAllMessages.Text = "Retrieve messages from PLC";
            this.toolTip1.SetToolTip(this.btnRetrieveAllMessages, "Retrieve all massages from all machineparts (will take some minutes)");
            this.btnRetrieveAllMessages.UseVisualStyleBackColor = true;
            this.btnRetrieveAllMessages.Click += new System.EventHandler(this.btnRetrieveAllMessages_Click);
            // 
            // btnRetrieveMachinepartRepo
            // 
            this.btnRetrieveMachinepartRepo.Location = new System.Drawing.Point(3, 2);
            this.btnRetrieveMachinepartRepo.Name = "btnRetrieveMachinepartRepo";
            this.btnRetrieveMachinepartRepo.Size = new System.Drawing.Size(172, 28);
            this.btnRetrieveMachinepartRepo.TabIndex = 50;
            this.btnRetrieveMachinepartRepo.Text = "Retrieve parts from PLC";
            this.toolTip1.SetToolTip(this.btnRetrieveMachinepartRepo, "Retrieve the machine parts from PLC (quick)");
            this.btnRetrieveMachinepartRepo.UseVisualStyleBackColor = true;
            this.btnRetrieveMachinepartRepo.Click += new System.EventHandler(this.btnRetrieveMachinepartRepo_Click);
            // 
            // lblModuleDefPlc
            // 
            this.lblModuleDefPlc.AutoSize = true;
            this.lblModuleDefPlc.Location = new System.Drawing.Point(664, 3);
            this.lblModuleDefPlc.Name = "lblModuleDefPlc";
            this.lblModuleDefPlc.Size = new System.Drawing.Size(16, 13);
            this.lblModuleDefPlc.TabIndex = 67;
            this.lblModuleDefPlc.Text = "...";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(608, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 65;
            this.label9.Text = "PLC Def:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusMessage,
            this.toolStripProgressBar,
            this.toolStripItemCount,
            this.toolStripStatusResolve,
            this.toolStripStatusVersion});
            this.statusStrip1.Location = new System.Drawing.Point(0, 626);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1054, 22);
            this.statusStrip1.TabIndex = 68;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusMessage
            // 
            this.toolStripStatusMessage.Name = "toolStripStatusMessage";
            this.toolStripStatusMessage.Size = new System.Drawing.Size(124, 17);
            this.toolStripStatusMessage.Text = "Not connected to PLC";
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // toolStripItemCount
            // 
            this.toolStripItemCount.Name = "toolStripItemCount";
            this.toolStripItemCount.Size = new System.Drawing.Size(80, 17);
            this.toolStripItemCount.Text = "0 parts, 0 msg";
            // 
            // toolStripStatusResolve
            // 
            this.toolStripStatusResolve.BackColor = System.Drawing.SystemColors.ControlLight;
            this.toolStripStatusResolve.Name = "toolStripStatusResolve";
            this.toolStripStatusResolve.Size = new System.Drawing.Size(93, 17);
            this.toolStripStatusResolve.Text = "Not resolved yet";
            // 
            // toolStripStatusVersion
            // 
            this.toolStripStatusVersion.Name = "toolStripStatusVersion";
            this.toolStripStatusVersion.Size = new System.Drawing.Size(40, 17);
            this.toolStripStatusVersion.Text = "0.0.0.0";
            // 
            // splitContainerTables
            // 
            this.splitContainerTables.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerTables.Location = new System.Drawing.Point(0, 0);
            this.splitContainerTables.Name = "splitContainerTables";
            this.splitContainerTables.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerTables.Panel1
            // 
            this.splitContainerTables.Panel1.Controls.Add(this.dataGridViewMachParts);
            // 
            // splitContainerTables.Panel2
            // 
            this.splitContainerTables.Panel2.Controls.Add(this.dataGridViewMsg);
            this.splitContainerTables.Size = new System.Drawing.Size(1052, 315);
            this.splitContainerTables.SplitterDistance = 156;
            this.splitContainerTables.TabIndex = 73;
            // 
            // dataGridViewMachParts
            // 
            this.dataGridViewMachParts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMachParts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMachParts.EnableHeadersVisualStyles = false;
            this.dataGridViewMachParts.FilterMachinePart = null;
            this.dataGridViewMachParts.Font = new System.Drawing.Font("Arial Narrow", 8F);
            this.dataGridViewMachParts.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewMachParts.Name = "dataGridViewMachParts";
            this.dataGridViewMachParts.RowHeadersWidth = 20;
            this.dataGridViewMachParts.Size = new System.Drawing.Size(1052, 156);
            this.dataGridViewMachParts.TabIndex = 71;
            this.dataGridViewMachParts.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // dataGridViewMsg
            // 
            this.dataGridViewMsg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMsg.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewMsg.Name = "dataGridViewMsg";
            this.dataGridViewMsg.RowHeadersWidth = 20;
            this.dataGridViewMsg.Size = new System.Drawing.Size(1052, 155);
            this.dataGridViewMsg.TabIndex = 72;
            // 
            // splitContainerMain
            // 
            this.splitContainerMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainerMain.Location = new System.Drawing.Point(0, 190);
            this.splitContainerMain.Name = "splitContainerMain";
            this.splitContainerMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerMain.Panel1
            // 
            this.splitContainerMain.Panel1.Controls.Add(this.splitContainerTables);
            // 
            // splitContainerMain.Panel2
            // 
            this.splitContainerMain.Panel2.Controls.Add(this.txtOutput);
            this.splitContainerMain.Size = new System.Drawing.Size(1052, 434);
            this.splitContainerMain.SplitterDistance = 315;
            this.splitContainerMain.TabIndex = 74;
            // 
            // txtOutput
            // 
            this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOutput.Location = new System.Drawing.Point(0, 0);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(1052, 115);
            this.txtOutput.TabIndex = 0;
            this.txtOutput.WordWrap = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnLoopupPlcMsgInDatabase);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblLatestDefId);
            this.panel1.Controls.Add(this.grpPlcConnection);
            this.panel1.Controls.Add(this.pnlPlcRelated);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.lblModuleDefPlc);
            this.panel1.Location = new System.Drawing.Point(3, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1045, 84);
            this.panel1.TabIndex = 76;
            // 
            // btnLoopupPlcMsgInDatabase
            // 
            this.btnLoopupPlcMsgInDatabase.Location = new System.Drawing.Point(379, 51);
            this.btnLoopupPlcMsgInDatabase.Name = "btnLoopupPlcMsgInDatabase";
            this.btnLoopupPlcMsgInDatabase.Size = new System.Drawing.Size(211, 28);
            this.btnLoopupPlcMsgInDatabase.TabIndex = 73;
            this.btnLoopupPlcMsgInDatabase.Text = "Lookup PLC messages to the database";
            this.toolTip1.SetToolTip(this.btnLoopupPlcMsgInDatabase, "Lookup PLC messages to the database");
            this.btnLoopupPlcMsgInDatabase.UseVisualStyleBackColor = true;
            this.btnLoopupPlcMsgInDatabase.Click += new System.EventHandler(this.BtnLoopupPlcMsgInDatabase_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.numGotoMachinePartId);
            this.groupBox1.Controls.Add(this.numGotoMachinePartDefId);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(867, 32);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(178, 52);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lookup";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(81, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 13);
            this.label4.TabIndex = 72;
            this.label4.Text = "MachinepartDefId:";
            // 
            // numGotoMachinePartDefId
            // 
            this.numGotoMachinePartDefId.Location = new System.Drawing.Point(91, 28);
            this.numGotoMachinePartDefId.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numGotoMachinePartDefId.Name = "numGotoMachinePartDefId";
            this.numGotoMachinePartDefId.Size = new System.Drawing.Size(69, 20);
            this.numGotoMachinePartDefId.TabIndex = 5;
            this.numGotoMachinePartDefId.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 72;
            this.label3.Text = "MachinepartId:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(756, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "LatestDefId:";
            // 
            // lblLatestDefId
            // 
            this.lblLatestDefId.AutoSize = true;
            this.lblLatestDefId.Location = new System.Drawing.Point(822, 3);
            this.lblLatestDefId.Name = "lblLatestDefId";
            this.lblLatestDefId.Size = new System.Drawing.Size(16, 13);
            this.lblLatestDefId.TabIndex = 69;
            this.lblLatestDefId.Text = "...";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(48, 48);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonReadFile,
            this.toolStripButtonWriteFile,
            this.toolStripButtonClear,
            this.toolStripButtonLogFile,
            this.toolStripButtonHelp});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1054, 55);
            this.toolStrip1.TabIndex = 75;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonReadFile
            // 
            this.toolStripButtonReadFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonReadFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonReadFile.Image")));
            this.toolStripButtonReadFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonReadFile.Name = "toolStripButtonReadFile";
            this.toolStripButtonReadFile.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonReadFile.Text = "toolStripButton1";
            this.toolStripButtonReadFile.ToolTipText = "Read PLC data from file";
            this.toolStripButtonReadFile.Click += new System.EventHandler(this.toolStripButtonReadFile_Click);
            // 
            // toolStripButtonWriteFile
            // 
            this.toolStripButtonWriteFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonWriteFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonWriteFile.Image")));
            this.toolStripButtonWriteFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonWriteFile.Name = "toolStripButtonWriteFile";
            this.toolStripButtonWriteFile.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonWriteFile.Text = "toolStripButton1";
            this.toolStripButtonWriteFile.ToolTipText = "Save PLC data to file";
            this.toolStripButtonWriteFile.Click += new System.EventHandler(this.toolStripButtonWriteFile_Click);
            // 
            // toolStripButtonClear
            // 
            this.toolStripButtonClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClear.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClear.Image")));
            this.toolStripButtonClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClear.Name = "toolStripButtonClear";
            this.toolStripButtonClear.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonClear.Text = "toolStripButton1";
            this.toolStripButtonClear.ToolTipText = "Clear all lists";
            this.toolStripButtonClear.Click += new System.EventHandler(this.toolStripButtonClear_Click);
            // 
            // toolStripButtonHelp
            // 
            this.toolStripButtonHelp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonHelp.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonHelp.Image")));
            this.toolStripButtonHelp.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonHelp.Name = "toolStripButtonHelp";
            this.toolStripButtonHelp.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonHelp.Text = "Help";
            this.toolStripButtonHelp.Click += new System.EventHandler(this.toolStripButtonHelp_Click);
            // 
            // ctrlFilterMachinePart1
            // 
            this.ctrlFilterMachinePart1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ctrlFilterMachinePart1.DifCat = 0;
            this.ctrlFilterMachinePart1.DifMsgUnresolved = 0;
            this.ctrlFilterMachinePart1.DifPartUnresolved = 0;
            this.ctrlFilterMachinePart1.FilterPathWildcard = "*";
            this.ctrlFilterMachinePart1.FilterShowCatDiff = false;
            this.ctrlFilterMachinePart1.FilterShowNoMessages = false;
            this.ctrlFilterMachinePart1.FilterShowUnresolvedMsg = false;
            this.ctrlFilterMachinePart1.FilterShowUnresolvedPart = false;
            this.ctrlFilterMachinePart1.FilterSortedColumnWildcard = "*";
            this.ctrlFilterMachinePart1.FilterWildcardCaseSensitive = false;
            this.ctrlFilterMachinePart1.Location = new System.Drawing.Point(2, 143);
            this.ctrlFilterMachinePart1.Name = "ctrlFilterMachinePart1";
            this.ctrlFilterMachinePart1.SelectedSort = PlcMachineParts.SortMachinePart.MachinePath;
            this.ctrlFilterMachinePart1.Size = new System.Drawing.Size(1050, 45);
            this.ctrlFilterMachinePart1.TabIndex = 77;
            this.toolTip1.SetToolTip(this.ctrlFilterMachinePart1, "Apply filter [F9]");
            this.ctrlFilterMachinePart1.FilterParametersChanged += new System.EventHandler<System.EventArgs>(this.ctrlFilterMachinePart1_FilterParametersChanged);
            // 
            // cmachinepartsBindingSource
            // 
            this.cmachinepartsBindingSource.DataSource = typeof(AI.App.PlcMachineParts.DataModel.ec_machineparts);
            // 
            // bplcsettingsvaluesBindingSource
            // 
            this.bplcsettingsvaluesBindingSource.DataSource = typeof(AI.App.PlcMachineParts.DataModel.ec_machineparts);
            // 
            // toolStripButtonLogFile
            // 
            this.toolStripButtonLogFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLogFile.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonLogFile.Image")));
            this.toolStripButtonLogFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLogFile.Name = "toolStripButtonLogFile";
            this.toolStripButtonLogFile.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonLogFile.Text = "toolStripButton1";
            this.toolStripButtonLogFile.Click += new System.EventHandler(this.toolStripButtonLogFile_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1054, 648);
            this.Controls.Add(this.ctrlFilterMachinePart1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainerMain);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "FrmMain";
            this.Text = "PLC Machine Parts";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.numGotoMachinePartId)).EndInit();
            this.grpPlcConnection.ResumeLayout(false);
            this.grpPlcConnection.PerformLayout();
            this.pnlPlcRelated.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainerTables.Panel1.ResumeLayout(false);
            this.splitContainerTables.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTables)).EndInit();
            this.splitContainerTables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMachParts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMsg)).EndInit();
            this.splitContainerMain.Panel1.ResumeLayout(false);
            this.splitContainerMain.Panel2.ResumeLayout(false);
            this.splitContainerMain.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerMain)).EndInit();
            this.splitContainerMain.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGotoMachinePartDefId)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmachinepartsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bplcsettingsvaluesBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.BindingSource bplcsettingsvaluesBindingSource;
		private System.Windows.Forms.BindingSource cmachinepartsBindingSource;
		private System.Windows.Forms.NumericUpDown numGotoMachinePartId;
		private Controls.Comboboxes.ComboModuleDefinition comboModuleDefinition1;
		private System.Windows.Forms.GroupBox grpPlcConnection;
		private System.Windows.Forms.ComboBox cmbAdsAddress;
		private System.Windows.Forms.Label lblAdsAddress;
		private System.Windows.Forms.CheckBox chkPlcConnection;
		private System.Windows.Forms.Label lblPlcState;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Panel pnlPlcRelated;
		private System.Windows.Forms.Button btnRetrieveAllMessages;
		private System.Windows.Forms.Button btnRetrieveMachinepartRepo;
		private System.Windows.Forms.Label lblModuleDefPlc;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusMessage;
		private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
		private System.Windows.Forms.ToolStripStatusLabel toolStripItemCount;
		private DataGridViewMachineParts dataGridViewMachParts;
		private DataGridViewMsg dataGridViewMsg;
		private System.Windows.Forms.SplitContainer splitContainerTables;
		private System.Windows.Forms.SplitContainer splitContainerMain;
		private System.Windows.Forms.TextBox txtOutput;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusResolve;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ToolStripButton toolStripButtonReadFile;
		private System.Windows.Forms.ToolStripButton toolStripButtonWriteFile;
		private System.Windows.Forms.ToolStripButton toolStripButtonClear;
		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusVersion;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label lblLatestDefId;
		private AI.App.PlcMachineParts.Controls.CtrlFilterMachinePart ctrlFilterMachinePart1;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.ToolStripButton toolStripButtonHelp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLoopupPlcMsgInDatabase;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numGotoMachinePartDefId;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripButton toolStripButtonLogFile;
    }
}

