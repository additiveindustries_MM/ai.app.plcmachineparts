﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AI.App.PlcMachineParts.Controls;
using AI.App.PlcMachineParts.DataModel;
using AI.Lib.CommandInterface.Host;
using AI.Lib.DatabaseConstants;
using AI.Lib.EnterpriseServerDefines.InterfacesBusiness;
using AI.Lib.Global;
using PlcMachineParts.CiFile;
using PlcMachineParts.Collections;

namespace PlcMachineParts
{
	public partial class FrmMain : Form
	{
        public FrmMain()
		{
			InitializeComponent();
            dataGridViewMachParts.FilterMachinePart = ctrlFilterMachinePart1;
		}

		private void Form1_Load(object sender, EventArgs e)
        {
            toolStripStatusVersion.Text = $"  Version:{Assembly.GetExecutingAssembly().GetName().Version.ToString()}";;
			var path = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;

            string[] adsAddresses = AI.App.PlcMachineParts.Properties.Settings.Default.ServerAddresses.Split(new char[]{'|'},  StringSplitOptions.RemoveEmptyEntries);
			cmbAdsAddress.Items.Clear();
			cmbAdsAddress.Items.AddRange(adsAddresses);
			if (AI.App.PlcMachineParts.Properties.Settings.Default.ServerAddressIndex < cmbAdsAddress.Items.Count)
				cmbAdsAddress.SelectedIndex = AI.App.PlcMachineParts.Properties.Settings.Default.ServerAddressIndex;

            comboModuleDefinition1.Fill(false);
			dataGridViewMachParts.SetDataSourceMachinePart();

			Program.Plc.OnMachinePartPlcArrived += CiFileDataRaw_OnMachinePartPlcArrived;
			Program.Plc.OnMessagePlcArrived += CiFileDataRaw_OnMessagePlcArrived;

			Program.Plc.OnModuleDefinitionChanged += Plc_OnModuleDefinitionChanged;
			Program.Plc.OnStateChanged += Plc_OnStateChanged;
			Program.Plc.OnReadMessagesPlcProgress += Plc_OnReadMessagesPlcProgress;
		}

		private void SetDataSourceMessages(bool clear = false)
		{
			if (clear)
				dataGridViewMsg.DataSource = null;
			else
			{
				if (dataGridViewMachParts.SelectedCells.Count > 0)
				{
					MachinePartPlc selected = (MachinePartPlc)dataGridViewMachParts.Rows[dataGridViewMachParts.SelectedCells[0].RowIndex].DataBoundItem;
					dataGridViewMsg.DataSource = selected.Messages.OrderBy(x => x.MessageId).ToList();
				}
			}
		}

		private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			Program.Plc.OnMachinePartPlcArrived -= CiFileDataRaw_OnMachinePartPlcArrived;
			Program.Plc.OnMessagePlcArrived -= CiFileDataRaw_OnMessagePlcArrived;

			Program.Plc.OnModuleDefinitionChanged -= Plc_OnModuleDefinitionChanged;
			Program.Plc.OnStateChanged -= Plc_OnStateChanged;
			Program.Plc.OnReadMessagesPlcProgress -= Plc_OnReadMessagesPlcProgress;

			Program.Plc.Close();
			Program.Plc.Shutdown();

			string itm = String.Join("|", cmbAdsAddress.Items.Cast<string>());
			AI.App.PlcMachineParts.Properties.Settings.Default.ServerAddresses = itm;
			AI.App.PlcMachineParts.Properties.Settings.Default.ServerAddressIndex = cmbAdsAddress.SelectedIndex;
            AI.App.PlcMachineParts.Properties.Settings.Default.Save();
		}

		private void dataGridView1_SelectionChanged(object sender, EventArgs e)
		{
			SetDataSourceMessages();
		}

		private void btnGotoMachinePartId_Click(object sender, EventArgs e)
		{
			if (!GotoMachinePart((long)numGotoMachinePartId.Value))
                Log.LogWarning($"Unknown Machine part id: {numGotoMachinePartId.Value}");
		}
        private void BtnGotoMachinePartDefId_Click(object sender, EventArgs e)
        {
            if (!GotoMachinePart((long) numGotoMachinePartDefId.Value, true))
            {
                bool hasResolved = dataGridViewMachParts.Rows
                                       .Cast<DataGridViewRow>()
                                       .Count(r => ((MachinePartPlc) r.DataBoundItem).MachinePartDefinitionId > 0) > 0;

                if (hasResolved)
                    Log.LogWarning($"Unknown Machine part DEFINITION id: {numGotoMachinePartDefId.Value}");
                else
                    Log.LogWarning($"Data not yet resolved");
            }
        }

        public bool GotoMachinePart(long id, bool isDefId = false)
		{
		    dataGridViewMachParts.ClearSelection();
            DataGridViewRow row = null;
            if (isDefId)
            {
                row = dataGridViewMachParts.Rows
                    .Cast<DataGridViewRow>()
                    .FirstOrDefault(r => ((MachinePartPlc)r.DataBoundItem).MachinePartDefinitionId == id);
            }
            else
            {
                row = dataGridViewMachParts.Rows
                    .Cast<DataGridViewRow>()
                    .FirstOrDefault(r => ((MachinePartPlc)r.DataBoundItem).MachinePartId == id);
            }

            if (row != null)
			{
				row.Selected = true;
				row.Cells[0].Selected = true;
				dataGridViewMachParts.FirstDisplayedScrollingRowIndex = dataGridViewMachParts.SelectedRows[0].Index;
                return true;
            }
            return false;
        }

		private void chkPlcConnection_CheckedChanged(object sender, EventArgs e)
		{}
		private void chkPlcConnection_Click(object sender, EventArgs e)
		{
			if (!chkPlcConnection.Checked)
			{
				ModuleDefinition md = comboModuleDefinition1.Selected;
				ModuleType mt = Global.GetAnyModuleType(md);
				Program.Plc.Init(RunMode.Real, cmbAdsAddress.Text + ":851", mt);
				Program.Plc.Open();
			}
			else
			{
				Program.Plc.OnModuleDefinitionChanged -= Plc_OnModuleDefinitionChanged;
				Program.Plc.Close();
				Program.Plc.Shutdown();
			}
			SetPlcButtons();
		}

		private void CiFileDataRaw_OnMessagePlcArrived(object sender, MessagePlcEventArgs e)
		{
			if (this.InvokeRequired && !this.Disposing) // Avoid problems with cross threading
			{
				this.BeginInvoke(new EventHandler<MessagePlcEventArgs>(this.CiFileDataRaw_OnMessagePlcArrived), new object[] { sender, e });
				return;
			}

			foreach (MessagePlc message in e.Messages)
			{
				if (dataGridViewMachParts.AllMachineParts.Any(x => x.MachinePartId == message.MachinePartId ))
                    dataGridViewMachParts.AllMachineParts.First(x => x.MachinePartId == message.MachinePartId).Messages.Add(message);
				else
					Log.LogWarning($"Message has no machinepart: {message}");
			}
			SetStatusBarCount();
		}

		private void CiFileDataRaw_OnMachinePartPlcArrived(object sender, MachinePartPlcEventArgs e)
		{
			if (this.InvokeRequired && !this.Disposing) // Avoid problems with cross threading
			{
				this.BeginInvoke(new EventHandler<MachinePartPlcEventArgs>(this.CiFileDataRaw_OnMachinePartPlcArrived), new object[] { sender, e });
				return;
			}

            dataGridViewMachParts.AllMachineParts.AddRange(e.MachineParts);
			SetStatusBarCount();
		}

		private void SetStatusBarCount()
		{
			toolStripItemCount.Text = $"{dataGridViewMachParts.CountAllMachineParts} parts, {dataGridViewMachParts.CountAllMessages} msg";
			ctrlFilterMachinePart1.DifCat = dataGridViewMachParts.AllMachineParts.Sum(x => x.MessagesCategoryDiff);
			ctrlFilterMachinePart1.DifMsgUnresolved = dataGridViewMachParts.AllMachineParts.Sum(x => x.CountMessagesUnResolved);
			ctrlFilterMachinePart1.DifPartUnresolved = dataGridViewMachParts.AllMachineParts.Count(x => !(x.HasMachinePart && x.HasMachinePartDefinition));
            dataGridViewMachParts.SetDataSourceMachinePart();
		}

		#region StatusBar
		public void SetStatusMsg(StatusMsg _status, string _msg)
		{
			switch (_status)
			{
				case StatusMsg.Info:	toolStripStatusMessage.Image = AI.App.PlcMachineParts.Properties.Resources.MsgInfo;	break;
				case StatusMsg.Warning:	toolStripStatusMessage.Image = AI.App.PlcMachineParts.Properties.Resources.MsgWarning;	break;
				case StatusMsg.Error:	toolStripStatusMessage.Image = AI.App.PlcMachineParts.Properties.Resources.MsgError;	break;
				default:
					throw new ArgumentOutOfRangeException(nameof(_status), _status, null);
			}
			toolStripStatusMessage.Text = String.IsNullOrEmpty(_msg)?"-":_msg;
		}
		public void SetStatusMsg()
		{
			toolStripStatusMessage.Image = AI.App.PlcMachineParts.Properties.Resources.MsgNone;
			toolStripStatusMessage.Text = "-";
		}
		public void SetProgress(bool _visible, int _value)
		{
			toolStripProgressBar.Value = MathFuncs.Clamp(_value, 0, 100);
			toolStripProgressBar.Visible = _visible;
		}
		#endregion

		private void Plc_OnModuleDefinitionChanged(object sender, EventArgs e)
		{
			if (this.InvokeRequired && !this.Disposing) // Avoid problems with cross threading
			{
				this.BeginInvoke(new EventHandler<EventArgs>(this.Plc_OnModuleDefinitionChanged), new object[] { sender, e });
				return;
			}

			lblModuleDefPlc.Text = Program.Plc.CurrentModuleDefinition.ToString();
		}
		private void Plc_OnStateChanged(object sender, EventArgs e)
		{
			if (this.InvokeRequired && !this.Disposing) // Avoid problems with cross threading
			{
				this.BeginInvoke(new EventHandler<EventArgs>(this.Plc_OnStateChanged), new object[] { sender, e });
				return;
			}

			SetPlcButtons();
		}

		private void Plc_OnReadMessagesPlcProgress(object sender, ReadMessagesPlcProgressEventArgs e)
		{
			if (this.InvokeRequired && !this.Disposing) // Avoid problems with cross threading
			{
				this.BeginInvoke(new EventHandler<ReadMessagesPlcProgressEventArgs>(this.Plc_OnReadMessagesPlcProgress), new object[] { sender, e });
				return;
			}

			Debug.WriteLine($"Progress: {e.ProgressCount}, {e.Ready}");

			Program.MainForm.SetProgress(!e.Ready, e.ProgressCount);
			dataGridViewMachParts.Refresh();
			dataGridViewMsg.Refresh();

			if (e.Ready)
			{
				Debug.WriteLine($"Ready");
				if (e.ProgressCount == 100)
					Program.MainForm.SetStatusMsg(StatusMsg.Info, "All messages received");
				else
					Program.MainForm.SetStatusMsg(StatusMsg.Error, $"Probably not all messages received: {e.ProgressCount}%");
			}
			else
			{
				Program.MainForm.SetStatusMsg(StatusMsg.Info, $"{e.ProgressCount}% received");
			}
		}

		private void SetPlcButtons()
		{
			lblPlcState.Text = Program.Plc.State.ToString();
			switch (Program.Plc.State)
			{
				case ConnecionState.Closed:
					chkPlcConnection.Enabled = true;
					chkPlcConnection.Checked = false;
					break;
				case ConnecionState.Opened:
					chkPlcConnection.Enabled = true;
					chkPlcConnection.Checked = true;
					break;
				case ConnecionState.Error:
					chkPlcConnection.Enabled = true;
					chkPlcConnection.Checked = false;
					break;
				case ConnecionState.Busy:
				case ConnecionState.Opening:
				case ConnecionState.Closing:
					chkPlcConnection.Enabled = false;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
			pnlPlcRelated.Enabled = (chkPlcConnection.Checked && chkPlcConnection.Enabled);
			btnRetrieveMachinepartRepo.Enabled = chkPlcConnection.Checked;
			btnRetrieveAllMessages.Enabled = chkPlcConnection.Checked;

			if (Program.Plc.State == ConnecionState.Error)
				Program.MainForm.SetStatusMsg(StatusMsg.Error, "Connection error");
			else
				Program.MainForm.SetStatusMsg(StatusMsg.Info, $"Plc {Program.Plc.State}");
		}

		private void btnRetrieveMachinepartRepo_Click(object sender, EventArgs e)
		{
			Program.AppData.LatestReadSource = "PLC";
			FillTreePlc();
		}

		private void btnRetrieveAllMessages_Click(object sender, EventArgs e)
		{
			Program.MainForm.SetProgress(true, 0);
			Program.Plc.StartRetrieveMessages(dataGridViewMachParts.CountAllMachineParts); // fires events
		}

		public void FillTreePlc()
		{
			if (DesignMode)
				return;

            dataGridViewMachParts.AllMachineParts = Program.Plc.GetMachineParts();
            dataGridViewMachParts.SetDataSourceMachinePart();
		}

		private void ctrlFilterMachinePart1_FilterParametersChanged(object sender, EventArgs e)
		{
            dataGridViewMachParts.SetDataSourceMachinePart();
		}

		private void Resolve()
		{
            dataGridViewMachParts.SetDataSourceMachinePart(true);
			SetDataSourceMessages(true);

			lblLatestDefId.Text = Program.AppData.FreeMachinePartDefinitionId.ToString();

			this.Enabled = false;
            SettingsDbEntities _context = new SettingsDbEntities();

			foreach (MachinePartPlc machinePart in dataGridViewMachParts.AllMachineParts)
			{
				if (machinePart.MachinePartId == 27010)
					Debug.WriteLine($"fdfdf");

				// Resolve the default messages. No dependancies, so no resolved values needed
				ResolveDefaultMessages(_context, machinePart);

				try
				{
					// Lookup the machinepart in the database
					machinePart.DbMachinePart = _context.ec_machineparts.FirstOrDefault(x => x.MACHINEPART_ID == machinePart.MachinePartId && x.PLC_MODULEDEF_ID == (long)machinePart.DefMod);
					if (machinePart.DbMachinePart != null)
					{
						// Found in db, look up definition
						machinePart.DbMachinePartDefinition = _context.ec_machinepartdefinitions.FirstOrDefault(x => x.MACHINEPARTDEFINITION_ID == machinePart.DbMachinePart.MACHINEPARTDEFINITION_ID);

						if (machinePart.DbMachinePartDefinition != null)
						{
							// Now see if we find a parent
							ResolveParent(_context, machinePart);


							ResolveSpecificMessages(_context, machinePart);
						}
						else
						{
							Log.LogError($"Machinepartdefinition not found in database!!!!! [{machinePart.MachinePath}][{machinePart.MachinePartName}] ({machinePart.MachinePartId})");
						}
					}
					else
					{
						Log.LogError($"Machinepart not found in database!!!!! [{machinePart.MachinePath}][{machinePart.MachinePartName}] ({machinePart.MachinePartId})");
						if(FindParentViaPath(machinePart.MachinePath, out MachinePartPlc parent))
						{
							Log.LogError($" --  machprt:   [{machinePart.MachinePath}]   [{machinePart.MachinePartName}] ({machinePart.MachinePartId})");
							Log.LogError($" --  parent:    [{parent?.MachinePath??"ROOT"}]        [{parent?.MachinePartName??"ROOT"}] ({parent?.MachinePartId??0})  defId={parent?.MachinePartDefinitionId??0} ");
						}
					}
				}
				catch (Exception ex)
				{
					Log.LogError($"Exception when resolving {machinePart} -> {ex.Message}");
				}
			}
			this.Enabled = true;
			SetStatusBarCount();
		}

		private void ResolveDefaultMessages(SettingsDbEntities _context, MachinePartPlc machinePart)
		{
			foreach (MessagePlc message in machinePart.Messages.Where(x => x.MessageId < 20))
			{
				if (message.MessageId < 20)
				{
					message.DbMessage = _context.ec_machinepartdefinitionmessages.FirstOrDefault(x =>
						x.MACHINEPARTDEFINITION_ID == 0 &&
						x.MESSAGE_ID == message.MessageId);
				}
			}
		}

		private void ResolveSpecificMessages(SettingsDbEntities _context, MachinePartPlc machinePart)
		{
			foreach (MessagePlc message in machinePart.Messages)
			{
				if (message.MessageId >= 20)
				{
					try
					{
						message.DbMessage = _context.ec_machinepartdefinitionmessages.FirstOrDefault(x =>
							x.MACHINEPARTDEFINITION_ID == machinePart.DbMachinePart.MACHINEPARTDEFINITION_ID &&
							x.MESSAGE_ID == message.MessageId);
					}
					catch (Exception ex)
					{
						Log.LogError($"Cannot resolve message {machinePart} -> {ex.Message}");
					}
				}
			}
		}

		private bool ResolveParent(SettingsDbEntities _context, MachinePartPlc machinePart)
		{
			if (machinePart.MachinePartParentId != 0)
			{
				ec_machineparts prnt = _context.ec_machineparts.FirstOrDefault(x => x.MACHINEPART_ID == machinePart.DbMachinePart.PARENTMACHINEPART_ID &&
																					x.PLC_MODULEDEF_ID == machinePart.DbMachinePart.PLC_MODULEDEF_ID);
				if (prnt != null)
				{
					// Find the MachinePartPlc in which the c_machineparts is present
					machinePart.MachinePartParent = dataGridViewMachParts.AllMachineParts.FirstOrDefault(x =>
						x.ModDefDbMachinePart == (ModuleDefinition)prnt.MODULEDEF_ID &&
						x.ModDefDbMachinePartPlc == (ModuleDefinition)prnt.PLC_MODULEDEF_ID &&
						x.MessageOffset == prnt.MSGOFFSET &&
						x.AssemblyCode == prnt.ASSEMBLYCODE &&
						x.MachinePartId == prnt.MACHINEPART_ID);

					if (machinePart.MachinePartParent == null)
						Log.LogWarning($"No success for parent lookup in table {machinePart}");
				}
				else
					Log.LogWarning($"No parent found in context for {machinePart}");
			}
			else if (machinePart.MachinePartParentId == 0 && machinePart.MachinePath.Count(ch => ch == '/') > 1)
			{
				// Based on path
				Log.LogWarning($"Part [{machinePart.MachinePath}][{machinePart.MachinePartName}] ({machinePart.MachinePartId}) no parent. Try to resolve by path");

				if (FindParentViaPath(machinePart.MachinePath, out MachinePartPlc parent))
				{
					if (parent == null)
					Log.LogMessage($"Path is head: {machinePart}");
					machinePart.MachinePartParent = parent;
				}
				else
				{
					Log.LogError($"Error parent lookup by path {machinePart}");
				}
			}
			return machinePart.MachinePartParent != null;
		}

		private bool FindParentViaPath(string path, out MachinePartPlc parent)
		{
			string p = path.TrimEnd(new[] {' ', '/'});
			if (p.LastIndexOf('/') > 0)
			{
				p = path.Substring(0, p.LastIndexOf('/')) + '/';
				parent = dataGridViewMachParts.AllMachineParts.FirstOrDefault(x => x.MachinePath.Equals(p));
				return true;
			}
			else if (p.LastIndexOf('/') == 0)
			{
				parent = null;
				return true; // head, no parent
			}
			parent = null;
			return false;
		}

		private void CreateSqlModifications()
		{
			int cntMsg = 0;
			int cntMpDef = 0;
			int cntMp = 0;
			StringBuilder sbMachinePartDef = new StringBuilder();
			StringBuilder sbMachinePart = new StringBuilder();
			StringBuilder sbMessage = new StringBuilder();
			StringBuilder sbMessageDiff = new StringBuilder();
            sbMessageDiff.AppendLine("-- Category/severity differences. Check if they are allowed");

			List<long> addedDefs = new List<long>();
			foreach (MachinePartPlc mp in dataGridViewMachParts.AllMachineParts)
			{
				if (!mp.Messages.Any(x => x.MessageId > 19)) // If machinepart only has messages 1, 2, 3, 4 we don't need it in the db
					continue;

				long defIdForThisRecord; 
				if (mp.DbMachinePart == null)
				{
					cntMp++;
					cntMpDef++;
					defIdForThisRecord = Program.AppData.FreeMachinePartDefinitionId;
					lblLatestDefId.Text = Program.AppData.FreeMachinePartDefinitionId.ToString();
					addedDefs.Add(defIdForThisRecord);
					Program.AppData.FreeMachinePartDefinitionId++;
					if (sbMachinePart.Length == 0)
					{
						sbMachinePart.AppendLine("-- ====Todo====");
						sbMachinePart.AppendLine("-- Each new record in 'c_machineparts' complete record: ***ASSEMBLYCODE, ***MSGOFFSET");
						sbMachinePart.AppendLine("-- New record in 'c_machinepartsdefinitions' the record should be complete, Only correct the DISPLAYNAME if needed");
						sbMachinePart.AppendLine("-- Each new record in 'c_machinepartdefinitionmessages': complete record: '***SHORT NAME', '***FULL TEXT'");
						sbMachinePart.AppendLine();
					}

					// Lookup if there are machineparts with the same name, so we can re-use the machinepart definition 
					if (dataGridViewMachParts.AllMachineParts.Any(x => x.DefMod == mp.DefMod && x.MachinePartName == mp.MachinePartName && !x.MachinePath.Equals(mp.MachinePath)))
					{
						sbMachinePart.AppendLine($"-- Machineparts in this module with the same name [{mp.MachinePartName}]:");
						foreach(MachinePartPlc same in dataGridViewMachParts.AllMachineParts.Where(x => x.DefMod == mp.DefMod && x.MachinePartName == mp.MachinePartName  && !x.MachinePath.Equals(mp.MachinePath)))
						{
							sbMachinePart.AppendLine($"--     machprtdefid:{same.MachinePartDefinitionId} path:{same.MachinePath} parent:{same.MachinePartParentId} Assy:{same.AssemblyCode} Offset:{same.MessageOffset} #msg:{same.CountMessages}");
						}
						sbMachinePart.AppendLine($"-- you might use the same machinepartdefinitionId");
						sbMachinePart.AppendLine();
					}
					else
					{
						sbMachinePart.AppendLine("-- Machinepart not used before");
						sbMachinePart.AppendLine();
					}


					if (mp.HasMachinePartParent)
					{
						sbMachinePart.AppendLine();
						sbMachinePart.AppendLine($"-- New part ({mp.MachinePartId})[{mp.MachinePath}] defId:{mp.MachinePartDefinitionId}");
						sbMachinePart.AppendLine($"-- Parent: ({mp.MachinePartParent?.MachinePartId ?? 0})[{mp.MachinePartParent?.MachinePath ?? "ROOT"}] [{mp.MachinePartParent?.MachinePartName ?? "ROOT"}] defId={mp.MachinePartParent?.MachinePartDefinitionId ?? 0} ");
					}
					else
					{
						// no parent found via definition. Try to lookup via path
						if(FindParentViaPath(mp.MachinePath, out MachinePartPlc parent))
						{
							sbMachinePart.AppendLine();
							sbMachinePart.AppendLine($"-- New part [{mp.MachinePath}]. According the path tis should be: ");
							sbMachinePart.AppendLine($"-- parent: ({parent?.MachinePartId??0})[{parent?.MachinePath??"ROOT"}] [{parent?.MachinePartName??"ROOT"}] defId={parent?.MachinePartDefinitionId??0} ");
						}
						else
						{
							sbMachinePart.AppendLine($"-- New part [{mp.MachinePath}] did not find a valid PARENTMACHINEPART_ID yet. Please complete manual");
						}
					}

					sbMachinePart.AppendLine($"INSERT INTO `c_machineparts` " +
					                         "(`MODULEDEF_ID`, `MACHINEPART_ID`, `PLC_MODULEDEF_ID`, `PARENTMACHINEPART_ID`, `MACHINEPARTDEFINITION_ID`, `ASSEMBLYCODE`, `MSGOFFSET`) VALUES " + Environment.NewLine +
					                         $"       ('{(int)comboModuleDefinition1.Selected}', '{mp.MachinePartId}', " +
					                         $"'{(int)comboModuleDefinition1.Selected}', '{mp.MachinePartParentId}', '{defIdForThisRecord}'," +
					                         $" '***ASSEMBLYCODE', '***MSGOFFSET');");

					if (sbMachinePartDef.Length == 0)
					{
						sbMachinePartDef.AppendLine("-- For each new record in c_machineparts please check the displayname (NAME is retrieved from PLC so should be ok)");
					}
					sbMachinePartDef.AppendLine($"INSERT INTO `c_machinepartdefinitions` (`MODULEDEF_ID`, `MACHINEPARTDEFINITION_ID`, `NAME`, `DISPLAYNAME`) VALUES " +
					                            $" ('{(int)comboModuleDefinition1.Selected}', '{defIdForThisRecord}', '{mp.MachinePartName}', '{mp.MachinePartName.FromCamelCase()}');");
				}
				else
					defIdForThisRecord = mp.MachinePartDefinitionId;

				//------Messages----------------------------------------------------------------------------------------------
				bool setComment = false;  // Comment with machinepart details only once per machinepart
				foreach (MessagePlc message in mp.Messages)
				{
					if (message.Resolved)
					{
						if (message.PlcMsgCategory != message.DbMsgCategory)
							sbMessageDiff.AppendLine(
								$"-- MessageId:{message.MessageId} defId:{mp.MachinePartDefinitionId} mpid:{mp.MachinePartId} [{mp.MachinePath}] " +
								$"Plc:({(int)message.PlcMsgCategory}){message.PlcMsgCategory} <>  Db:({(int) message.DbMsgCategory}){message.DbMsgCategory}  " +
								$"short[{message.MsgShortName}] full[{message.MsgFullText}] Descr:[{message.Descr}]");
					}
					else
					{
						cntMsg++;
						if (!setComment)
						{
							sbMessage.AppendLine();
							sbMessage.AppendLine($"-- New messages for machinepart partId={mp.MachinePartId} partDefId={defIdForThisRecord} path=[{mp.MachinePath}] name=[{mp.MachinePartName}]");
							setComment = true;
						}
						sbMessage.AppendLine($"INSERT INTO `c_machinepartdefinitionmessages` (`MACHINEPARTDEFINITION_ID`, `MESSAGE_ID`, `MESSAGECAT_ID`, `SHORTNAME`, `FULLTEXT`) VALUES " +
						                     $"('{defIdForThisRecord}', '{message.MessageId}', '{(int)message.PlcMsgCategory}', " +
						                     $"'{(!String.IsNullOrEmpty(message.MsgShortName)?message.MsgShortName:"***SHORT NAME")}', " +
						                     $"'{(!String.IsNullOrEmpty(message.MsgFullText)?message.MsgFullText:"***FULL TEXT")}');");
					}
				}
			} // end loop collect

			string src = Program.AppData.LatestReadSource.ToLower().Equals("plc") ? $"PLC:{cmbAdsAddress.Text}:851 ({comboModuleDefinition1.Selected})" : $"File:{Program.AppData.LatestReadSource}";
			txtOutput.Text = $"-- Module: {lblModuleDefPlc.Text} From: {src}{Environment.NewLine}";

			if (cntMsg + cntMp + cntMpDef == 0)
			{
				txtOutput.Text += "-- No changes found" + Environment.NewLine;
			}
			else
			{
				txtOutput.Text += $"-- !!!!Please lookup all fields marked with *** in this text and resolve them!!!!{Environment.NewLine}";

				if (addedDefs.Count > 0)
					txtOutput.Text += $"-- Found {cntMp} new machinepart{(cntMp>1?"s":"")}, {cntMpDef} new machinepartdefinition{(cntMpDef>1?"s":"")} containing messages.{Environment.NewLine}";
				else
					txtOutput.Text += $"-- Found no new machineparts or machinepartdefinitions containing messages.{Environment.NewLine}";

				if (sbMessage.Length > 0)
					txtOutput.Text += $"-- {cntMsg} new message{(cntMsg>1?"s":"")}{Environment.NewLine}{Environment.NewLine}";
				else
					txtOutput.Text += $"-- found no new messages{Environment.NewLine}{Environment.NewLine}";

				if (addedDefs.Count > 0)
				{
					txtOutput.Text += sbMachinePart.ToString();
					txtOutput.Text += Environment.NewLine;
					txtOutput.Text += sbMachinePartDef.ToString();
					txtOutput.Text += Environment.NewLine;
				}
				if (sbMessage.Length > 0)
					txtOutput.Text += sbMessage.ToString() + Environment.NewLine;
			}
			if (sbMessageDiff.Length > 0)
				txtOutput.Text += Environment.NewLine + sbMessageDiff.ToString() + Environment.NewLine;

			txtOutput.Text += $"-- ================================================================================================================{Environment.NewLine}{Environment.NewLine}";

			toolStripStatusResolve.Text = $"{cntMp} new mp{(cntMp>1?"s":"")}, {cntMpDef} new mpdef{(cntMpDef>1?"s":"")}, {cntMsg} new msg{(cntMsg>1?"s":"")}";
		}

		#region Buttons strip
		private void toolStripButtonReadFile_Click(object sender, EventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog(); 
			openFileDialog.Filter = "Xml files (*.xml)|*.xml|All files (*.*)|*.*";
			openFileDialog.RestoreDirectory = true;
			openFileDialog.Title = "Open CI RAW file" ;
			if (openFileDialog.ShowDialog() == DialogResult.OK)
			{
				Program.AppData.LatestReadSource = openFileDialog.FileName;
                dataGridViewMachParts.AllMachineParts = Program.Plc.CiFileDataRaw.Read(openFileDialog.FileName);
                dataGridViewMachParts.SetDataSourceMachinePart();
			}
			SetStatusBarCount();
            MachinePartPlc part = dataGridViewMachParts.AllMachineParts.FirstOrDefault();
            if (part != null)
            {
                comboModuleDefinition1.Selected = part.DefMod;
            }
            else
            {
                comboModuleDefinition1.Selected = ModuleDefinition.Undefined;
            }

			toolStripStatusResolve.Text = "Not resolved yet";
		}
		private void toolStripButtonWriteFile_Click(object sender, EventArgs e)
		{
			if (dataGridViewMachParts.AllMachineParts?.Count > 0)
			{
				SaveFileDialog saveFileDialogRaw = new SaveFileDialog();
				saveFileDialogRaw.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
				saveFileDialogRaw.RestoreDirectory = true;
				saveFileDialogRaw.Title = "Save RAW CI data to file";

				if (saveFileDialogRaw.ShowDialog(this) == DialogResult.OK)
					Program.Plc.CiFileDataRaw.Write(dataGridViewMachParts.AllMachineParts, saveFileDialogRaw.FileName);
			}
			else
			{
				MessageBox.Show($"Nothing to write here", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
		private void toolStripButtonLookup_Click(object sender, EventArgs e)
		{
		}
		private void toolStripButtonClear_Click(object sender, EventArgs e)
		{
            dataGridViewMachParts.SetDataSourceMachinePart(true);
			SetDataSourceMessages(true);
			txtOutput.Text = "";
            dataGridViewMachParts.AllMachineParts.Clear();
			Program.AppData.LatestReadSource = "";
            dataGridViewMachParts.SetDataSourceMachinePart();
			toolStripStatusResolve.Text = "Not resolved yet";
			SetStatusBarCount();
		}
		#endregion

		private void FrmMain_KeyUp(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
                case Keys.Enter:
                    Control activeControl = this.ActiveControl;
                    if (activeControl == numGotoMachinePartId)
                        GotoMachinePart((long)numGotoMachinePartId.Value, false);
                    else if (activeControl == numGotoMachinePartDefId)
                        GotoMachinePart((long)numGotoMachinePartDefId.Value, true);

                    break;
				case Keys.F9:
                    dataGridViewMachParts.SetDataSourceMachinePart();
					break;
			}
		}

		private void toolStripButtonHelp_Click(object sender, EventArgs e)
		{
			FrmHelp frm = new FrmHelp();
			frm.ShowDialog(this);
		}

        private void BtnLoopupPlcMsgInDatabase_Click(object sender, EventArgs e)
        {
            if (dataGridViewMachParts.AllMachineParts?.Count > 0)
            {
                this.Enabled = false;
                SplashForm.ShowSplashScreen("Please wait for the database lookup");
                Resolve();
                CreateSqlModifications();
                SplashForm.CloseForm();
                this.Enabled = true;
            }
            else
            {
                MessageBox.Show($"Nothing to lookup here", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void toolStripButtonLogFile_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Program.LogFile) && File.Exists(Program.LogFile))
                Process.Start(Program.LogFile);
            else
                MessageBox.Show($"No log file found [{Program.LogFile}].", "No log", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }
	}
}
