﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using AI.Lib.DatabaseConstants;
using AI.Lib.Global;

namespace PlcMachineParts
{
    public enum SortMachinePart
    {
        MachinePath,
        MachinePartId,
        MachinePartDefinitionId,
        MachinePartName,
		AssemblyCode,
		Parent,
        NoSort
	}


	public static class Global
	{
		public static List<T> ToObjectList<T>(this DataGridViewRowCollection rows)
		{
			return (from DataGridViewRow cada in rows select cada.DataBoundItem).Select(x => (T)x).ToList();
		}

		internal static ModuleType GetAnyModuleType(ModuleDefinition _def)
		{
			switch (_def)
			{
				case ModuleDefinition.Undefined:
					return ModuleType.Undefined;
				case ModuleDefinition.CTM:
					return ModuleType.CTM;
				case ModuleDefinition.EXP:
					return ModuleType.EXP;
				case ModuleDefinition.ROB:
					return ModuleType.ROB;
				case ModuleDefinition.EXC:
					return ModuleType.EXC;
				case ModuleDefinition.AMC:
					return ModuleType.AMC1; // Just pick one
				case ModuleDefinition.STM:
					return ModuleType.STM1; // Just pick one
				case ModuleDefinition.HTM:
					return ModuleType.HTM1; // Just pick one
				case ModuleDefinition.PRM:
					return ModuleType.PRM1; // Just pick one
				case ModuleDefinition.Area:
					return ModuleType.Area1; // Just pick one
				default:
					throw new ArgumentOutOfRangeException(nameof(_def), _def, null);
			}
		}

        public static string ModuleDefinitionToString(long modDefId)
        {
            if (Enum.IsDefined(typeof(ModuleDefinition), (byte)modDefId))
            {
                ModuleDefinition a = (ModuleDefinition)modDefId;
                return a.ToString();
            }
            else if (modDefId == 70)
            {
                return "PLT";
            }
            return "???";
        }


		/// <summary>
		/// Converts a string from CamelCase to a human readable format. 
		/// Inserts spaces between upper and lower case letters. 
		/// Also strips the leading "_" character, if it exists.
		/// </summary>
		/// <param name="propertyName"></param>
		/// <returns>A human readable string.</returns>
		public static string FromCamelCase(this string propertyName)
		{
			string returnValue = String.IsNullOrEmpty(propertyName)?"":propertyName;
			//Strip leading "_" character
			returnValue = Regex.Replace(returnValue, "^_", "").Trim(); 
			//Add a space between each lower case character and upper case character
			returnValue = Regex.Replace(returnValue, "([a-z])([A-Z])", "$1 $2").Trim(); 
			//Add a space between 2 upper case characters when the second one is followed by a lower space character
			returnValue = Regex.Replace(returnValue, "([A-Z])([A-Z][a-z])", "$1 $2").Trim(); 
			return returnValue;
		}

		public static long CountLinesInString(string s)
		{
			long count = 1;
			int start = 0;
			while ((start = s.IndexOf('\n', start)) != -1)
			{
				count++;
				start++;
			}
			return count;
		}

		/// <summary> Splits the multilines in a textbbox into a list of strings </summary>
		public static List<string> SplitLines(this TextBox txtBox)
		{
			bool continueProcess = true;
			int i = 1; //Zero Based So Start from 1
			int j = 0;
			List<string> lines = new List<string>();
			while (continueProcess)
			{
				var index = txtBox.GetFirstCharIndexFromLine(i);
				if (index != -1)
				{
					lines.Add(txtBox.Text.Substring(j, index - j));
					j = index;
					i++;
				}
				else
				{
					lines.Add(txtBox.Text.Substring(j, txtBox.Text.Length - j));
					continueProcess = false;
				}
			}
			return lines;
		}
	}

	
	public static class GridViewWxtensions
	{
		private static long NL(object field)
		{
			if (field == null)
			{ return -1; }
			else if (field == System.DBNull.Value)
			{return -2;}

			return Convert.ToInt64(field);
		}
		private static int NI(object field)
		{
			if (field == null)
			{ return -1; }
			else if (field == System.DBNull.Value)
			{return -2;}

			return Convert.ToInt32(field);
		}



		public static int SelectedIndex(this DataGridView grid)
		{
			return grid.CurrentCell.RowIndex;
		}

		public static long GetSelectedUniqueKey(this DataGridView grid)
		{
			return grid.GetSelected("SETTINGS_KEY_UNIQUE");
		}
		public static long GetSelectedKey(this DataGridView grid)
		{
			return grid.GetSelected("SETTINGS_KEY");
		}
		private static long GetSelected(this DataGridView grid, string fieldName)
		{
			if (grid?.CurrentCell != null)
			{
				int row = grid.CurrentCell.RowIndex;

				long uniqueKey = NL(grid.Rows[row].Cells[fieldName].Value);
				return uniqueKey;
			}
			return -1;
		}

		public static bool SetSelectedUniqueKey(this DataGridView grid, long uniqueKey)
		{
			return grid.SetSelected("SETTINGS_KEY_UNIQUE", uniqueKey);
		}
		public static bool SetSelectedKey(this DataGridView grid, long key)
		{
			return grid.SetSelected("SETTINGS_KEY_UNIQUE", key);
		}
		private static bool SetSelected(this DataGridView grid, string fieldName, long uniqueKey)
		{
			if (grid?.CurrentCell != null)
			{
				DataGridViewRow row = grid.Rows.Cast<DataGridViewRow>()
					.First(r => r.Cells[fieldName].Value.ToString().Equals(uniqueKey.ToString()));
				row.Selected = true;
				grid.FirstDisplayedScrollingRowIndex = grid.SelectedRows[0].Index;
				return true;
			}
			return false;
		}

		public static bool GetSelected(this DataGridView grid, out int row, out int col)
		{
			row = -1;
			col = -1;

			if (grid.SelectedRows?.Count > 0)
			{
				row = grid.SelectedRows[0].Index;
				col = 0;
				return true;
			}
			else if (grid.SelectedCells?.Count > 0)
			{
				row = grid.SelectedCells[0].RowIndex;
				col = grid.SelectedCells[0].ColumnIndex;
				return true;
			}
			return false;
		}

		public static bool SetSelected(this DataGridView grid, int row, int col)
		{
			grid.ClearSelection();
			if (grid != null && grid.Rows.Count > 0 && row >= 0)
			{
				int indexRow = MathFuncs.Clamp(row, 0, grid.Rows.Count - 1);
				int indexCol = MathFuncs.Clamp(col, 0, grid.Columns.Count - 1);
				grid.Rows[indexRow].Cells[indexCol].Selected = true;
				grid.Rows[indexRow].Selected = true;
				return true;
			}
			else
			{
				return false;
			}
		}

		public static DataGridViewColumn SetColumn(this DataGridView grid, string colName, string header, int width, int index, DataGridViewContentAlignment headAlign = DataGridViewContentAlignment.MiddleLeft, bool bold = false)
		{
			if (grid.Columns.Contains(colName))
			{
				grid.Columns[colName].HeaderCell.Style.Alignment = headAlign;
				grid.Columns[colName].Visible = true; // If we give it extra parms we want to see it
				grid.Columns[colName].AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.NotSet;
				grid.Columns[colName].Width = width;
				grid.Columns[colName].HeaderText = header;
				grid.Columns[colName].DisplayIndex = index;
				if (bold)
				{
					grid.Columns[colName].DefaultCellStyle.Font = new Font(grid.DefaultCellStyle.Font, FontStyle.Bold);
					grid.Columns[colName].HeaderCell.Style.Font = new Font(grid.DefaultCellStyle.Font, FontStyle.Bold);
				}
				return grid.Columns[colName];
			}
			return null;
		}

		/// <summary> Set Width, with field precensce check </summary>
		public static void SetColumnWidth(this DataGridView grid, string colName, int width)
		{
			if(grid.Columns.Contains(colName))
				grid.Columns[colName].Width = width;
		}
		/// <summary> Set Header, with field precensce check </summary>
		public static void SetColumnHeader(this DataGridView grid, string colName, string header)
		{
			if(grid.Columns.Contains(colName))
				grid.Columns[colName].HeaderText = header;
		}
		/// <summary> Set Header, with field precensce check </summary>
		public static void SetColumnIndex(this DataGridView grid, string colName, int index)
		{
			if(grid.Columns.Contains(colName))
				grid.Columns[colName].DisplayIndex = index;
		}
		/// <summary> Set Visible, with field precensce check </summary>
		public static void SetColumnVisible(this DataGridView grid, string colName, bool visible)
		{
			if(grid.Columns.Contains(colName))
				grid.Columns[colName].Visible = visible;
		}
		/// <summary> Set Visible, with field precensce check </summary>
		public static void SetColumnAutoSizeMode(this DataGridView grid, string colName, DataGridViewAutoSizeColumnMode mode)
		{
			if(grid.Columns.Contains(colName))
				grid.Columns[colName].AutoSizeMode = mode;
		}

		public static void SetColumnsVisible(this DataGridView grid, bool visible)
		{
			grid.SetColumnsVisible(null, visible);
		}
		/// <summary> Set Visible, with field precensce check </summary>
		public static void SetColumnsVisible(this DataGridView grid, string[] colNameVisible, bool visible = true)
		{
			foreach (DataGridViewColumn column in grid.Columns)
			{
				if (colNameVisible == null)
					column.Visible = visible;
				else if (colNameVisible.Contains(column.Name))
					column.Visible = visible;
				else
					column.Visible = !visible;
			}
		}

		public static List<T> ToObjectList<T>(this DataGridViewRowCollection rows)
		{
			return (from DataGridViewRow cada in rows select cada.DataBoundItem).Select(x => (T)x).ToList();
		}
	}
}