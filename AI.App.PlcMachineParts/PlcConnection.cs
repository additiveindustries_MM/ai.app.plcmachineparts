﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using AI.Lib.CommandInterface;
using AI.Lib.CommandInterface.CI;
using AI.Lib.CommandInterface.Host;
using AI.Lib.EnterpriseDefines.CommandInterface.Events;
using AI.Lib.EnterpriseServerDefines.InterfacesBusiness;
using AI.Lib.Global;
using PlcMachineParts.CiFile;
using AI.Lib.DatabaseConstants;


namespace PlcMachineParts
{
	public class PlcConnection
	{
		BackgroundWorker bgwGetTestMessages = new BackgroundWorker
		{
			WorkerSupportsCancellation = true
		};

		public ICommandInterfaceManager2 CiManager => ciHost?.CmdInterfacerMgr;

		private CommandInterfaceManagerHost ciHost = null;

		#region OnStateChanged
		public event EventHandler<EventArgs> OnStateChanged
		{
			add
			{
				if (ciHost?.CmdInterfacerMgr != null)
					ciHost.CmdInterfacerMgr.OnStateChanged += value;
			}
			remove
			{
				if (ciHost?.CmdInterfacerMgr != null)
					ciHost.CmdInterfacerMgr.OnStateChanged -= value;
			}
		}

		public ConnecionState State =>  ciHost?.CmdInterfacerMgr?.State ?? ConnecionState.Closed;
		#endregion

		#region OnMessagePlcArrived
		public event EventHandler<MessagePlcEventArgs> OnMessagePlcArrived;

		private void FireOnMessagePlcArrived(List<MessagePlc> _lst)
		{
			EventHandler<MessagePlcEventArgs> handler = OnMessagePlcArrived;
			handler?.Invoke(this, new MessagePlcEventArgs(_lst));
		}
		#endregion
		#region OnOnMachinePartPlcArrived
		public event EventHandler<MachinePartPlcEventArgs> OnMachinePartPlcArrived;

		private void FireOnMachinePartPlcArrived(List<MachinePartPlc> _lst)
		{
			EventHandler<MachinePartPlcEventArgs> handler = OnMachinePartPlcArrived;
			handler?.Invoke(this, new MachinePartPlcEventArgs(_lst));
		}
		#endregion

		#region OnReadMessagesPlcProgress
		public event EventHandler<ReadMessagesPlcProgressEventArgs> OnReadMessagesPlcProgress;

		private void FireOnReadMessagesPlcProgress(int _progressPercent, bool _ready = false)
		{
			EventHandler<ReadMessagesPlcProgressEventArgs> handler = OnReadMessagesPlcProgress;
			handler?.Invoke(this, new ReadMessagesPlcProgressEventArgs(_progressPercent, _ready));
		}
		#endregion

		#region OnCurrentModuleDefinition
		public event EventHandler<EventArgs> OnModuleDefinitionChanged;
		internal void FireOnModuleDefinitionChanged()
		{
			EventHandler<EventArgs> handler = OnModuleDefinitionChanged;
			handler?.Invoke(this, new EventArgs());
		}
		private ModuleDefinition __currentModuleDefinition = ModuleDefinition.Undefined;
		public ModuleDefinition CurrentModuleDefinition
		{
			get { return __currentModuleDefinition; }
			set
			{
				if (__currentModuleDefinition != value)
				{
					__currentModuleDefinition = value;
					FireOnModuleDefinitionChanged();
				}
			}
		}
		#endregion

		#region OnPlcTimeChange
		public event EventHandler<SyncTimeReceivedEventArgs> OnSyncTimeReceived
		{
			add => CiManager.OnSyncTimeReceived += value;
			remove => CiManager.OnSyncTimeReceived -= value;
		}
		#endregion

		#region OnWhatsNewChange
		public event EventHandler<WhatsNewEventArgs> OnWhatsNewChange;

		private void FireOnWhatsNewChange(ushort _cntMsg, ushort _cntEvent, ushort _cntBlDbChanges)
		{
			EventHandler<WhatsNewEventArgs> handler = OnWhatsNewChange;
			handler?.Invoke(this, new WhatsNewEventArgs(_cntMsg, _cntEvent, _cntBlDbChanges));
		}
		#endregion

		public RunMode CiType {get { return ciHost != null && CiManager != null ? CiManager.RunMode : RunMode.Disabled; }}
		public string Address { get; private set; }

		public CiFileRaw CiFileDataRaw { get; private set; } = null;


		public bool IsInited { get; private set; } = false;

		public PlcConnection()
		{
			CiFileDataRaw = new CiFileRaw();
		}

		public void Init(RunMode _type, string address, ModuleType _module)
		{
			Address = address;
			ciHost = new CommandInterfaceManagerHost(254,1, _module);
			ciHost.Init(Address, 60000, RunMode.Real);
			ciHost.InternetAllowed = false;
			//ciHost.CmdInterfacerMgr.WhatsNewEnabled = false;
			try
			{
				CiManager.OnActiveMachineMessagesReceived += CiManager_OnActiveMachineMessagesReceived;
				CiManager.OnHistoricalMachineMessagesReceived += CiManager_OnHistoricalMachineMessagesReceived;
				bgwGetTestMessages.DoWork += bgwGetMessages_OnDoWork;
				bgwGetTestMessages.RunWorkerAsync(null);
			}
			catch (Exception ex)
			{
				Log.Message(Severity.ERROR, "StartRetrieveMessages() exception: " + ex.Message);
				//State = ConnecionState.Error;
			}
			IsInited = true;
		}

		public void Open()
		{
			ciHost?.Open();
		}
		public void Close()
		{
			running = false;
			if (ciHost?.CmdInterfacerMgr.State == ConnecionState.Opened)
				ciHost?.Close();
		}
		public void Shutdown()
		{
			if (CiManager != null && CiManager.IsInited)
			{
				CiManager.OnActiveMachineMessagesReceived -= CiManager_OnActiveMachineMessagesReceived;
				CiManager.OnHistoricalMachineMessagesReceived -= CiManager_OnHistoricalMachineMessagesReceived;
				bgwGetTestMessages.CancelAsync();
			}

			if (ciHost != null)
			{
				ciHost.Shutdown();
				ciHost = null;
			}
			IsInited = false;
		}

		// Don't retrieve if already busy
		private bool retrieveTestMsgActive = false;
		// Only used for progress estimation
		private readonly List<uint> _retrievedMachineParts = new List<uint>(); 
		// Only used for progress estimation
		private long _numberOfMachinePartsToRetrieve = 0;

		public void StartRetrieveMessages(long numberOfMachineParts)
		{
			if (numberOfMachineParts == 0)
				return;

			_retrievedMachineParts.Clear();
			_numberOfMachinePartsToRetrieve = numberOfMachineParts;
			ManualResetEvent manualEvent = new ManualResetEvent(false);
			CiManager.HandleDebugOptions(CI_DebugOption.SendTestEvents, CI_DebugOption.None, (returnValue,currOptions) =>
				{
					Log.Message(Severity.MESSAGE, $"===========StartRetrieveMessages(): HandleDebugOptions returned {returnValue}, current options: {currOptions}");
					manualEvent.Set();
				});
			manualEvent.WaitOne();
			retrieveTestMsgActive = true;
		}

		private void CiManager_OnHistoricalMachineMessagesReceived(object sender, HistoricalMessagesReceivedEventArgs e)
		{
			Log.Message(Severity.MESSAGE, $"===========CiManager_OnHistoricalMachineMessagesReceived(): received {e.AllEvents.Count} historical msg");

			List<MessagePlc> msg = new List<MessagePlc>();
			foreach (EvtTest evt in e.AllEvents.Where(ev => ev.GetType() == typeof(EvtTest)).Cast<EvtTest>())
			{
				Debug.WriteLine($"Received Event: MP:{evt.MachinePartId} :{evt.TypeModule} mid:{evt.MessageId}");
				msg.Add(new MessagePlc(evt, CurrentModuleDefinition));

				// Just for progress calculation
				if (!_retrievedMachineParts.Contains(evt.MachinePartId)) 
					_retrievedMachineParts.Add(evt.MachinePartId);
			}
			FireOnMessagePlcArrived(msg);
			int percent = (int)Math.Round(MathFuncs.Clamp( (100F * (float)_retrievedMachineParts.Count / (float)_numberOfMachinePartsToRetrieve), 0, 100));
			FireOnReadMessagesPlcProgress(percent);
		}

		private void CiManager_OnActiveMachineMessagesReceived(object sender, MachineMessagesReceivedEventArgs e)
		{
			Log.Message(Severity.MESSAGE, $"===========CiManager_OnActiveMachineMessagesReceived(): received {e.AllMessages.Messages.Count} active msg");
		}

		private bool running = false;
		private void bgwGetMessages_OnDoWork(object sender, DoWorkEventArgs e)
		{
			running = true;
			while (running)
			{
				if (retrieveTestMsgActive)
				{
					CI_DebugOption currOptions = CI_DebugOption.None;
					ManualResetEvent manualEvent = new ManualResetEvent(false);
					CiManager.HandleDebugOptions(CI_DebugOption.None, CI_DebugOption.None, (returnValue,currOpt) =>
					{
						Log.Message(Severity.MESSAGE, $"bgwGetMessages_OnDoWork(): HandleDebugOptions returned {returnValue}, current options: {currOpt}");
						currOptions = currOpt;
						manualEvent.Set();
					});
					manualEvent.WaitOne();

					if (currOptions == CI_DebugOption.None)
					{
						Log.Message(Severity.MESSAGE, $"bgwGetMessages_OnDoWork(): DONE");
						retrieveTestMsgActive = false;
						FireOnReadMessagesPlcProgress(100,true); // ready
					}
				}
				Thread.Sleep(5000);
			}
		}

		public List<MachinePartPlc> GetMachineParts()
		{
			List<MachinePartPlc> lst = null;
			_retrievedMachineParts.Clear();
			ManualResetEvent manualEvent = new ManualResetEvent(false);
			CiManager.Get_MCF_MachinePartRepository((returnValue,repo) =>
			{
				string[] parts = repo.AsPresentableList().Split(new string[] { "\r\n", "\n\r", "\n" },  StringSplitOptions.RemoveEmptyEntries);
				lst = ParseMachineParts(parts);
				manualEvent.Set();
			});
			manualEvent.WaitOne();
			FireOnMachinePartPlcArrived(lst);
			return lst;
		}

		/// <summary> Just used for estimation percentage retrieved messages </summary>
		public Int32 NumberOfRetrievedMachineParts { get; private set;  } = 0;

		public List<MachinePartPlc> ParseMachineParts(string AllLines)
		{
			string[] parts = AllLines.Split(new string[] { "\r\n", "\n\r", "\n" },  StringSplitOptions.RemoveEmptyEntries);
			return ParseMachineParts(parts);
		}

		public List<MachinePartPlc> ParseMachineParts(string[] lines)
		{
			CurrentModuleDefinition = ModuleDefinition.Undefined;
			NumberOfRetrievedMachineParts = 0;
			List<MachinePartPlc> repoMachineParts = new List<MachinePartPlc>();

			foreach (string line in lines)
			{
				// Log.Message(Severity.MESSAGE, $"Line: {s}");
				if (line.TrimStart().StartsWith("##")) // ignore comment
					continue;

				if (!String.IsNullOrEmpty(line.TrimAll())) // filter empty lines
				{
					MachinePartPlc mp = new MachinePartPlc(line);
					if (repoMachineParts.Count == 0)
					{
						// first time only. First object will hopefully be ModuleDefinition
						CurrentModuleDefinition = mp.DefMod;
					}
					repoMachineParts.Add(mp);
				}
			}
			// Used for estimation
			NumberOfRetrievedMachineParts = repoMachineParts?.Count ?? 0;
			return repoMachineParts;
		}

		public List<MessagePlc> ParseMessages(string allLines)
		{
			string[] lines = allLines.Split(new string[] { "\r\n", "\n\r", "\n" },  StringSplitOptions.RemoveEmptyEntries);
			return ParseMessages(lines);
		}
		public List<MessagePlc> ParseMessages(string[] lines)
		{
			List<MessagePlc> messageList = new List<MessagePlc>();
			foreach (string line in lines)
			{
				if (line.TrimStart().StartsWith("##")) // ignore comment
					continue;

				try
				{
					MessagePlc m = new MessagePlc(line);
					messageList.Add(m);
				}
				catch (Exception)
				{
					Log.Message(Severity.ERROR, $"Parse error: {line}");
				}
			}
			return messageList;
		}
	}
}
