﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using AI.Lib.EnterpriseDefines;
using AI.Lib.Global;
using PlcMachineParts.Collections;
using PlcMachineParts.Data;

namespace PlcMachineParts
{
	static class Program
	{
		private static ApplicationData appData = null;
		public static ApplicationData AppData => appData;

        public static string LogFile => lf?.CurrentLogFileName??"";

        private static LoggerFile lf = null;
		private static LoggerDebug ld = null;

		private static PlcConnection plc = null;
		public static PlcConnection Plc => plc;

		private static FrmMain mainForm = null;
		public static FrmMain MainForm { get { return mainForm; }}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			#region Logging functionality
			ld = new LoggerDebug("PLC");// log to debug prompt

            string pth = "";

            try
            {
                pth = FileLocation.PathLogging;
                FileLocation.ResolveLocations(out string err);
            }
            catch (Exception)
            {
                string[] altPath = new[] { @"c:\Temp\", @"c:\Tmp\" };
                foreach (string s in altPath)
                {
                    if (Directory.Exists(s))
                    {
                        pth = s;
                        break;
                    }
                }
                if (String.IsNullOrEmpty(pth))
                {
                    Directory.CreateDirectory(altPath[0]);
                    pth = altPath[0];
                }
            }

            lf = new LoggerFile(pth, "PlcMachineParts", "PlcMachineParts", "PLC");
			lf.TimerFlush = true;
			Log.Message(Severity.MESSAGE, "Logging written to: [{0}]", lf.CurrentLogFileName);
			Log.Message(Severity.MESSAGE, FileLocation.PathRegistryOverride ? "Registry override for location:" : "Original locations set:");
			#endregion

			appData = new ApplicationData();
			plc = new PlcConnection();

			if (!ParseArgs(Environment.GetCommandLineArgs()))
			{
				MessageBox.Show($"Cannot handle commandline: [{Environment.CommandLine}]", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			mainForm = new FrmMain();
			Application.Run(mainForm);
		}

		private static bool ParseArgs(string[] args)
		{
			foreach (string s in args)
			{
				if (s.Length > 2 && new[]{'/','-'}.Contains(s[0])) // Starts with / or -
				{
					string[] parts = s.Split(new char[]{'='},  StringSplitOptions.RemoveEmptyEntries);

					if (parts[0].Substring(1).ToLower().StartsWith("data"))
					{
						if (parts?.Length == 2)
						{
							//Program.AppData.TargetDb = parts[1];
						}
						else
						{
							Log.LogError($"Cannot set database to [{s}]");
							return false;
						}
					}
				}
			}

			return true;
		}

		private static string ShowHelp()
		{
			return "AI.App.PlcMachineParts -database=[server|local]";
		}

	}
}
