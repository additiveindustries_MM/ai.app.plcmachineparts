﻿using System;
using System.Collections.Generic;
using PlcMachineParts.CiFile;

namespace PlcMachineParts
{
	public class MessagePlcEventArgs : EventArgs
	{
		public List<MessagePlc> Messages {get;set;}

		public MessagePlcEventArgs(List<MessagePlc> _lst)
		{
			Messages = _lst;
		}
	}
	public class MachinePartPlcEventArgs : EventArgs
	{
		public List<MachinePartPlc> MachineParts {get;set;}

		public MachinePartPlcEventArgs(List<MachinePartPlc> _lst)
		{
			MachineParts = _lst;
		}
	}

	// OnReadMessagesPlcProgress
	public class ReadMessagesPlcProgressEventArgs : EventArgs
	{
		public int ProgressCount {get; private set;}
		public bool Ready {get; private set;}

		public ReadMessagesPlcProgressEventArgs(int progressCount, bool _ready = false)
		{
			ProgressCount = progressCount;
			Ready = _ready;
		}
	}

	public class WhatsNewEventArgs : EventArgs
	{
		public ushort CntMsg { get; }
		public ushort CntEvent { get; }
		public ushort CntBlDbChanges { get; }

		public WhatsNewEventArgs(ushort _cntMsg, ushort _cntEvent, ushort _cntBlDbChanges)
		{
			CntMsg = _cntMsg;
			CntEvent = _cntEvent;
			CntBlDbChanges = _cntBlDbChanges;
		}
	}

	public class PlcTimeEventArgs : EventArgs
	{
		public DateTime TimeMCF {get;}
		public DateTime TimeMCP {get;}

		public PlcTimeEventArgs(DateTime _mcf, DateTime _mcp)
		{
			TimeMCF = _mcf;
			TimeMCP = _mcp;
		}
	}
}
